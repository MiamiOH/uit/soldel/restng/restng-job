<?php
/*
-----------------------------------------------------------
FILE NAME: getJobDepartmentCodeTest.php

Copyright (c) 2016 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Axhay Patel

DESCRIPTION:


ENVIRONMENT DEPENDENCIES:
RESTng Framework
PHPUnit

TABLE USAGE:

Web Service Usage:

AUDIT TRAIL:

DATE    PRJ-TSK          UniqueID
Description:

08/20/2016              patelah
Description:            Initial Draft

-----------------------------------------------------------
*/

namespace MiamiOH\RestngJob\Tests\Unit;

use MiamiOH\RESTng\App;

class GetJobDepartmentCodeTest extends \MiamiOH\RESTng\Testing\TestCase {

    /*************************/
    /**********Set Up*********/
    /*************************/
    private $mockApp, $request, $dbh, $user, $job, $queryallRecords ;


    // set up method automatically called by PHPUnit before every test method:

    protected function setUp() {
        //set up the mock api:
        $this->mockApp = $this->createMock(App::class);

        $this->mockApp->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        //set up the mock request:
        $this->request = $this->getMockBuilder('\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions'))
            ->getMock();

        //set up the mock dbh:
        $this->dbh = $this->getMockBuilder('\RESTng\Connector\Database\DBH')
            ->setMethods(array('queryall_array','prepare', 'queryfirstcolumn'))
            ->getMock();


        $OCI8 = $this->getMockBuilder('\RESTng\Legacy\DB\DBH\STH\OCI8')
            ->disableOriginalConstructor()
            ->setMethods(array('execute','fetchrow_assoc'))
            ->getMock();

        $this->dbh->method('prepare') -> willReturn($OCI8);

        $OCI8 -> method('execute') -> willReturn(1);

        $this->user = $this->getMockBuilder('\RESTng\Util\User')
            ->setMethods(array('isAuthorized'))
            ->getMock();

        $db = $this->getMockBuilder('\RESTng\Connector\Database')
            ->setMethods(array('getHandle'))
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

        $ds = $this->getMockBuilder('\RESTng\Connector\Datasource')
            ->setMethods(array('getDataSource'))
            ->getMock();

        //set up the service with the mocked out resources:
        $this->job = new \MiamiOH\RestngJob\Services\Job();

        $this->job->setApp($this->mockApp);
        $this->job->setApiUser($this->user);
        $this->job->setDatabase($db);
        //$this->job->setDatasource($ds);
        $this->job->setRequest($this->request);

    }


    /********************************************/
    /**********Department Code Tests****************/
    /*******************************************/

    /********** Department Code Single Tests *********/

    /**
     *  Test to get jobs by Department Code (Single)
     */

    public function testJobByDepartmentCode()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsDepartmentCode')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->expects($this->at(0))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQuerySingleDepartmentCode'
            )));
        //$this ->job-> method('camelCaseKeys') -> willReturnValue(1);
        $this->response = $this->job->getjobs();
        $payload = $this->response->getPayload();

        $this->assertEquals(\MiamiOH\RESTng\API_OK, $this->response->getStatus());
        $this->assertEquals($payload, $this->returnJobSingleDepartmentCode());

    }

    /**
     * Mock Options for Grade Level Single job Test
     */
    public function mockOptionsDepartmentCode()
    {
        return array('standardizedDepartmentCode' => 'ITMCS', 'includeSupplement' => 'True');
    }

    /**
     * Mock Database Return
     */
    public function mockQuerySingleDepartmentCode()
    {
        return array(
            array(
                'unique_Id' => "test1",                                              // unique Id
                'pidm' => '23432',                                                   // pidm
                'plus_Number' => '+34231',                                           // plusNumber
                'position_Id' => '995723',                                           // positionId
                'primary' => 'true',                                                 // primary
                'title' => 'Application Analyst',                                    // title
                'start_Date' => '2016-07-01',                                        // startDate
                'status' => 'A',                                                     // status
                'organization_Code' => 'MCS98',                                      // organizationCode
                'organization_Description' => "Information Technology Services",     // organization Description
                'classification_Code' => "AF",                                       // classification Code
                'classification_Description' => "Admin Fulltime",                    // classification Description
                'standardized_Department_Code' => 'ITMCS',                           // standardizedDepartmentCode
                'standardized_Department_Name' => 'IT Services',                    // standardizedDepartmentName
                'standardized_Division_Code' => 'CAS',                               // standardizedDivisionCode
                'instructor' => "false",                                             // instructor
            ),
        );
    }

    /**
     * Expected Return from Unique ID Test
     */
    public function returnJobSingleDepartmentCode()
    {
        return array(
            array(
                "uniqueId" => "test1",
                "pidm" => "23432",
                "plusNumber" => "+34231",
                "positionId" => "995723",
                "primary" => "true",
                "title" => "Application Analyst",
                "startDate" => "2016-07-01",
                "status" => "A",
                "organizationCode" => "MCS98",
                "organizationDescription" => "Information Technology Services",
                "classificationCode" => "AF",
                "classificationDescription" => "Admin Fulltime",
                "standardizedDepartmentCode" => "ITMCS",
                "standardizedDepartmentName" =>"IT Services",
                "standardizedDivisionCode" => "CAS",
                "instructor" => "false",
            )
        );
    }
    /**
     *  Invalid Department Code (Single)
     */

    public function testInvalidDepartmentCode()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsInvalidDepartmentCode')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());


        try {
            $this->response = $this->job->getjobs();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Invalid Standardized Department Code.", $e->getMessage());
        }

    }

    /**
     * Mock Options for Invalid Grade Level Single Test
     */

    public function mockOptionsInvalidDepartmentCode()
    {
        return array('standardizedDepartmentCode' => ['?P']);
    }

    /**
     *  SQL Injection Department Code (Single)
     */

    public function testSQLInjectionDepartmentCode()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsSQLInjectionDepartmentCode')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());


        try {
            $this->response = $this->job->getjobs();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Invalid Standardized Department Code.",
                $e->getMessage());
        }

    }

    /**
     * Mock Options for SQL Injection Grade Level Single Test
     */
    public function mockOptionsSQLInjectionDepartmentCode()
    {
        return array('standardizedDepartmentCode' => ['\';--']);
    }


    /********** Department Code Multiple Tests *********/

    /**
     *  Test to get jobs by Department Code (Multiple)
     */

    public function testJobsByDepartmentCodeMultiple()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsDepartmentCodeMultiple')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->expects($this->at(0))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryMultipleDepartmentCode'
            )));

        $this->dbh->expects($this->at(1))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryTotalMultiple'
            )));

        $this->response = $this->job->getJobs();
        $payload = $this->response->getPayload();

        $this->assertEquals(\MiamiOH\RESTng\API_OK, $this->response->getStatus());
        $this->assertEquals($payload, $this->returnjobMultipleDepartmentCode());

    }

    /**
     * Mock Options for Grade Level Multiple job Test
     */
    public function mockOptionsDepartmentCodeMultiple()
    {
        return array('standardizedDepartmentCode' => ['RDH9', 'ITMCS'], 'includeSupplement' =>'True');
    }

    /**
     * Mock Database Return for Multiple TenureEmployee Test
     */
    public function mockQueryMultipleDepartmentCode()
    {
        return array(
            array(
                'unique_Id' => "test1",                                              // unique Id
                'pidm' => '23432',                                                   // pidm
                'plus_Number' => '+34231',                                           // plusNumber
                'position_Id' => '995723',                                           // positionId
                'primary' => 'true',                                                 // primary
                'title' => 'Application Analyst',                                    // title
                'start_Date' => '2016-07-01',                                        // startDate
                'status' => 'A',                                                     // status
                'organization_Code' => 'MCS98',                                      // organizationCode
                'organization_Description' => "Information Technology Services",     // organization Description
                'classification_Code' => "AF",                                       // classification Code
                'classification_Description' => "Admin Fulltime",                    // classification Description
                'standardized_Department_Code' => 'RDH9',                            // standardizedDepartmentCode
                'standardized_Department_Name' => 'IT Services',                    // standardizedDepartmentName
                'standardized_Division_Code' => 'CAS',                               // standardizedDivisionCode
                'instructor' => "false",                                             // instructor
            ),
            array(
                'unique_Id' => "test1",                                              // unique Id
                'pidm' => '23432',                                                   // pidm
                'plus_Number' => '+34231',                                           // plusNumber
                'position_Id' => '995723',                                           // positionId
                'primary' => 'true',                                                 // primary
                'title' => 'Application Analyst',                                    // title
                'start_Date' => '2016-07-01',                                        // startDate
                'status' => 'A',                                                     // status
                'organization_Code' => 'MCS98',                                      // organizationCode
                'organization_Description' => "Information Technology Services",     // organization Description
                'classification_Code' => "AF",                                       // classification Code
                'classification_Description' => "Admin Fulltime",                    // classification Description
                'standardized_Department_Code' => 'ITMCS',                           // standardizedDepartmentCode
                'standardized_Department_Name' => 'IT Services',                    // standardizedDepartmentName
                'standardized_Division_Code' => 'FBS',                               // standardizedDivisionCode
                'instructor' => "false",                                             // instructor
            ),
        );
    }

    /**
     * Expected Return from Multiple Division Code Test
     */
    public function returnJobMultipleDepartmentCode()
    {
        return array(
            array(
                "uniqueId" => "test1",
                "pidm" => "23432",
                "plusNumber" => "+34231",
                "positionId" => "995723",
                "primary" => "true",
                "title" => "Application Analyst",
                "startDate" => "2016-07-01",
                "status" => "A",
                "organizationCode" => "MCS98",
                "organizationDescription" => "Information Technology Services",
                "classificationCode" => "AF",
                "classificationDescription" => "Admin Fulltime",
                "standardizedDepartmentCode" => "RDH9",
                "standardizedDepartmentName" =>"IT Services",
                "standardizedDivisionCode" => "CAS",
                "instructor" => "false",
            ),
            array(
                "uniqueId" => "test1",
                "pidm" => "23432",
                "plusNumber" => "+34231",
                "positionId" => "995723",
                "primary" => "true",
                "title" => "Application Analyst",
                "startDate" => "2016-07-01",
                "status" => "A",
                "organizationCode" => "MCS98",
                "organizationDescription" => "Information Technology Services",
                "classificationCode" => "AF",
                "classificationDescription" => "Admin Fulltime",
                "standardizedDepartmentCode" => "ITMCS",
                "standardizedDepartmentName" =>"IT Services",
                "standardizedDivisionCode" => "FBS",
                "instructor" => "false",
            ),

        );
    }

    /**
     *  Invalid Department Code (Multiple)
     */

    public function testInvalidDepartmentCodeMultiple()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsInvalidDepartmentCodeMultiple')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());


        try {
            $this->response = $this->job->getjobs();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Invalid Standardized Department Code.",
                $e->getMessage());
        }

    }

    /**
     * Mock Options for Invalid Grade Level Single Test
     */

    public function mockOptionsInvalidDepartmentCodeMultiple()
    {
        return array('standardizedDepartmentCode' => ['true', 'null', '#none']);
    }

    /**
     *  SQL Injection Department Code (Multiple)
     */

    public function testSQLInjectionDepartmentCodeMultiple()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsSQLInjectionDepartmentCodeMultiple')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());


        try {
            $this->response = $this->job->getjobs();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Invalid Standardized Department Code.",
                $e->getMessage());
        }

    }

    /**
     * Mock Options for SQL Injection Grade Level Multiple Test
     */

    public function mockOptionsSQLInjectionDepartmentCodeMultiple()
    {
        return array('standardizedDepartmentCode' => ['RHD9', '\';--', 'ITMCS']);
    }

}