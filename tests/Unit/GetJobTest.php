<?php

/*
-----------------------------------------------------------
FILE NAME: getJobTest.php

Copyright (c) 2016 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Emily Schmidt

DESCRIPTION:  Unit Tests for Testing the GET Functionality of
              the Job Web Service

ENVIRONMENT DEPENDENCIES: PHP Unit

AUDIT TRAIL:

DATE        UniqueID
09/07/2016  schmidee       Initial File
*/

namespace MiamiOH\RestngJob\Tests\Unit;

use MiamiOH\RESTng\App;

class GetJobTest extends \MiamiOH\RESTng\Testing\TestCase
{


    /*************************/
    /**********Set Up*********/
    /*************************/
    private $dbh, $request, $response, $bannerUtil, $job, $queryallRecords;

    // set up method which is automatically called by PHPUnit before every test method:
    protected function setUp()
    {

        $mockApp = $this->createMock(App::class);
        $mockApp->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        //set up the mock request:
        $this->request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array(
                'getResourceParam',
                'getOptions',
                'getSubObjects',
                'getResourceParamKey',
                'getOffset',
                'getLimit'
            ))
            ->getMock();


        //set up the mock dbh
        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('queryall_array', 'queryfirstcolumn'))
            ->getMock();


        $db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(array('getHandle'))
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

        /*
        $this->bannerUtil = $this->getMockBuilder('\MiamiOH\RESTng\Service\Extension\BannerUtil')
            ->setMethods(array('getId'))
            ->getMock();
        */

        $this->job = new \MiamiOH\RestngJob\Services\Job();

        $this->job->setApp($mockApp);
        $this->job->setDatabase($db);
        //$this->job->setBannerUtil($this->bannerUtil);
        $this->job->setRequest($this->request);

    }

    /**********************************/
    /**********Unique ID Tests*********/
    /**********************************/

    /********** Unique ID Single Tests *********/

    /**
     *  Test to get Jobs by Unique ID (Single)
     */

    public function testJobByUniqueID()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsUniqueID')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->expects($this->at(0))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQuerySingle'
            )));

        $this->dbh->expects($this->at(0))->method('queryfirstcolumn')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryTotalSingle'
            )));

        $this->response = $this->job->getJobs();
        $payload = $this->response->getPayload();

        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $this->response->getStatus());
        $this->assertEquals($payload, $this->returnJobSingle());

    }

    /**
     * Mock Options for Unique ID Single Job Test
     */
    public function mockOptionsUniqueID()
    {
        return array('uniqueId' => ['test1']);
    }

    /**
     * Mock Database Return for Single Job Test
     */
    public function mockQuerySingle()
    {
        return array(
            array(
                'pidm' => 00000000,
                'unique_Id' => 'TEST1',
                'plus_Number' => '+00000000',
                'start_Date' => '2016-01-01',
                'classification_Code' => 'FF',
                'classification_Description' => 'Faculty Full-time',
                'title' => "Assistant Professor",
                'organization_Code' => 'BIO99',
                'organization_Description' => 'Microbiology',
                'standardized_Division_Code' => 'FBS',
                'standardized_Division_Name' => 'Farmer School of Business',
                'standardized_Department_Code' => 'CSE',
                'standardized_Department_Name' => 'Computer Science & Software Engineering',
                'primary' => 'true',
                'position_Id' => '00000000',
                'status' => 'A',
                'instructor' => 'true',
                'contract_Status' => null,
                'rnum' => 1
            ),
        );
    }

    /**
     * Mock Database Return for Total Single Job Test
     */
    public function mockQueryTotalSingle()
    {
        return array(
            array(
                'total' => 1,
            ),
        );
    }

    /**
     * Expected Return from Unique ID Single Job Test
     */
    public function returnJobSingle()
    {
        return array(
            array(
                "pidm" => "00000000",
                "uniqueId" => "TEST1",
                "plusNumber" => "+00000000",
                "startDate" => "2016-01-01",
                "classificationCode" => "FF",
                "classificationDescription" => "Faculty Full-time",
                "title" => "Assistant Professor",
                'organizationCode' => 'BIO99',
                'organizationDescription' => 'Microbiology',
                'standardizedDivisionCode' => 'FBS',
                'standardizedDivisionName' => 'Farmer School of Business',
                'standardizedDepartmentCode' => 'CSE',
                'standardizedDepartmentName' => 'Computer Science & Software Engineering',
                'primary' => 'true',
                'positionId' => '00000000',
                'status' => 'A',
                'instructor' => 'true',
                'contractStatus' => null,

            )
        );
    }

    /**
     *  Invalid Unique ID (Single)
     */

    public function testInvalidUniqueID()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsInvalidUniqueID')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());


        try {
            $this->response = $this->job->getJobs();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Invalid Unique ID.",
                $e->getMessage());
        }

    }

    /**
     * Mock Options for Invalid Unique ID Single Test
     */
    public function mockOptionsInvalidUniqueID()
    {
        return array('uniqueId' => ['/*/*76']);
    }

    /**
     *  SQL Injection Unique ID (Single)
     */

    public function testSQLInjectionUniqueID()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsSQLInjectionUniqueID')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());


        try {
            $this->response = $this->job->getJobs();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Invalid Unique ID.",
                $e->getMessage());
        }

    }

    /**
     * Mock Options for SQL Injection Unique ID Single Test
     */
    public function mockOptionsSQLInjectionUniqueID()
    {
        return array('uniqueId' => ['\';--']);
    }

    /********** Unique ID Multiple Tests *********/

    /**
     *  Test to get Jobs by Unique ID (Multiple)
     */

    public function testJobByUniqueIDMultiple()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsUniqueIDMultiple')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->expects($this->at(0))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryMultiple'
            )));

        $this->dbh->expects($this->at(0))->method('queryfirstcolumn')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryTotalMultiple'
            )));

        $this->response = $this->job->getJobs();
        $payload = $this->response->getPayload();

        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $this->response->getStatus());
        $this->assertEquals($payload, $this->returnJobMultiple());

    }

    /**
     * Mock Options for Unique ID Multiple Job Test
     */
    public function mockOptionsUniqueIDMultiple()
    {
        return array('uniqueId' => ['test1', 'test2', 'test3']);
    }

    /**
     * Mock Database Return for Multiple Job Test
     */
    public function mockQueryMultiple()
    {
        return array(
            array(
                'pidm' => 00000000,
                'start_Date' => '2016-01-01',
                'unique_Id' => 'TEST1',
                'plus_Number' => '+00000000',
                'classification_Code' => 'FF',
                'classification_Description' => 'Faculty Full-time',
                'title' => "Assistant Professor",
                'organization_Code' => 'BIO99',
                'organization_Description' => 'Microbiology',
                'standardized_Division_Code' => 'FBS',
                'standardized_Division_Name' => 'Farmer School of Business',
                'standardized_Department_Code' => 'CSE',
                'standardized_Department_Name' => 'Computer Science & Software Engineering',
                'primary' => 'true',
                'position_Id' => '00000000',
                'status' => 'A',
                'instructor' => 'true',
                'contract_Status' => null,
                'rnum' => 1
            ),
            array(
                'pidm' => 00000001,
                'start_Date' => '2015-01-01',
                'unique_Id' => 'TESTUSER2',
                'plus_Number' => '+00000001',
                'classification_Code' => 'FF',
                'classification_Description' => 'Administrative Full-time',
                'title' => 'Associate Librarian',
                'organization_Code' => 'ENG99',
                'organization_Description' => 'English',
                'standardized_Division_Code' => 'CAS',
                'standardized_Division_Name' => 'College of Arts and Science',
                'standardized_Department_Code' => 'EDL',
                'standardized_Department_Name' => 'Educational Leadership',
                'primary' => 'false',
                'position_Id' => '00000001',
                'status' => 'R',
                'instructor' => 'false',
                'contract_Status' => null,
                'rnum' => 1
            ),
            array(
                'pidm' => 00000002,
                'classification_Code' => 'FF',
                'start_Date' => '2013-01-20',
                'unique_Id' => 'TESTUSER3',
                'plus_Number' => '+00000002',
                'classification_Description' => 'Administrative Full-time',
                'title' => 'Professor',
                'organization_Code' => 'ART99',
                'organization_Description' => 'Art',
                'standardized_Division_Code' => 'PHP',
                'standardized_Division_Name' => 'Physical Facilities',
                'standardized_Department_Code' => 'ENG',
                'standardized_Department_Name' => 'English',
                'primary' => 'true',
                'position_Id' => '00000000',
                'status' => 'A',
                'instructor' => 'true',
                'contract_Status' => null,
                'rnum' => 1
            ),
        );
    }

    /**
     * Mock Database Return for Total Multiple Job Test
     */
    public function mockQueryTotalMultiple()
    {
        return array(
            array(
                'total' => 3,
            ),
        );
    }

    /**
     * Expected Return from Multiple Job Test
     */
    public function returnJobMultiple()
    {
        return array(
            array(
                "pidm" => "00000000",
                "uniqueId" => "TEST1",
                "plusNumber" => "+00000000",
                "startDate" => "2016-01-01",
                "classificationCode" => "FF",
                "classificationDescription" => "Faculty Full-time",
                "title" => "Assistant Professor",
                'organizationCode' => 'BIO99',
                'organizationDescription' => 'Microbiology',
                'standardizedDivisionCode' => 'FBS',
                'standardizedDivisionName' => 'Farmer School of Business',
                'standardizedDepartmentCode' => 'CSE',
                'standardizedDepartmentName' => 'Computer Science & Software Engineering',
                'primary' => 'true',
                'positionId' => '00000000',
                'status' => 'A',
                'instructor' => 'true',
                'contractStatus' => null,
            ),
            array(
                "pidm" => "00000001",
                "uniqueId" => "TESTUSER2",
                "plusNumber" => "+00000001",
                "startDate" => "2015-01-01",
                "classificationCode" => "FF",
                "classificationDescription" => "Administrative Full-time",
                "title" => "Associate Librarian",
                'organizationCode' => 'ENG99',
                'organizationDescription' => 'English',
                'standardizedDivisionCode' => 'CAS',
                'standardizedDivisionName' => 'College of Arts and Science',
                'standardizedDepartmentCode' => 'EDL',
                'standardizedDepartmentName' => 'Educational Leadership',
                'primary' => 'false',
                'positionId' => '00000001',
                'status' => 'R',
                'instructor' => 'false',
                'contractStatus' => null,
            ),
            array(
                "pidm" => "00000002",
                "uniqueId" => "TESTUSER3",
                "plusNumber" => "+00000002",
                "startDate" => "2013-01-20",
                "classificationCode" => "FF",
                "classificationDescription" => "Administrative Full-time",
                "title" => 'Professor',
                'organizationCode' => 'ART99',
                'organizationDescription' => 'Art',
                'standardizedDivisionCode' => 'PHP',
                'standardizedDivisionName' => 'Physical Facilities',
                'standardizedDepartmentCode' => 'ENG',
                'standardizedDepartmentName' => 'English',
                'primary' => 'true',
                'positionId' => '00000000',
                'status' => 'A',
                'instructor' => 'true',
                'contractStatus' => null,
            ),

        );
    }

    /**
     *  Invalid Unique ID (Multiple)
     */

    public function testInvalidUniqueIDMultiple()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsInvalidUniqueIDMultiple')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());


        try {
            $this->response = $this->job->getJobs();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Invalid Unique ID.",
                $e->getMessage());
        }

    }

    /**
     * Mock Options for Invalid Unique ID Single Test
     */
    public function mockOptionsInvalidUniqueIDMultiple()
    {
        return array('uniqueId' => ['test1', '/*/*76', 'test2']);
    }

    /**
     *  SQL Injection Unique ID (Multiple)
     */

    public function testSQLInjectionUniqueIDMultiple()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsSQLInjectionUniqueIDMultiple')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());


        try {
            $this->response = $this->job->getJobs();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Invalid Unique ID.",
                $e->getMessage());
        }

    }

    /**
     * Mock Options for SQL Injection Unique ID Multiple Test
     */
    public function mockOptionsSQLInjectionUniqueIDMultiple()
    {
        return array('uniqueId' => ['test1', '\';--', 'test2']);
    }

    /**********************************/
    /**********PIDM Tests**************/
    /**********************************/

    /********** PIDM Single Tests *********/

    /**
     *  Test to get Jobs by PIDM (Single)
     */

    public function testJobByPIDM()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsPIDM')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->expects($this->at(0))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQuerySingle'
            )));

        $this->dbh->expects($this->at(1))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryTotalSingle'
            )));

        $this->response = $this->job->getJobs();
        $payload = $this->response->getPayload();

        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $this->response->getStatus());
        $this->assertEquals($payload, $this->returnJobSingle());

    }

    /**
     * Mock Options for PIDM Single Job Test
     */
    public function mockOptionsPIDM()
    {
        return array('pidm' => ['00000000']);
    }

    /**
     *  Invalid PIDM (Single)
     */

    public function testInvalidPIDM()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsInvalidPIDM')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());


        try {
            $this->response = $this->job->getJobs();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Invalid pidm.",
                $e->getMessage());
        }

    }

    /**
     * Mock Options for Invalid PIDM Single Test
     */
    public function mockOptionsInvalidPIDM()
    {
        return array('pidm' => ['test1']);
    }

    /**
     *  SQL Injection PIDM (Single)
     */

    public function testSQLInjectionPIDM()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsSQLInjectionPIDM')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());


        try {
            $this->response = $this->job->getJobs();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Invalid pidm.",
                $e->getMessage());
        }

    }

    /**
     * Mock Options for SQL Injection PIDM Single Test
     */
    public function mockOptionsSQLInjectionPIDM()
    {
        return array('pidm' => ['\';--']);
    }

    /********** PIDM Multiple Tests *********/

    /**
     *  Test to get Jobs by PIDM (Multiple)
     */

    public function testJobByPIDMMultiple()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsPIDMMultiple')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->expects($this->at(0))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryMultiple'
            )));

        $this->dbh->expects($this->at(1))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryTotalMultiple'
            )));

        $this->response = $this->job->getJobs();
        $payload = $this->response->getPayload();

        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $this->response->getStatus());
        $this->assertEquals($payload, $this->returnJobMultiple());

    }

    /**
     * Mock Options for PIDM Multiple Job Test
     */
    public function mockOptionsPIDMMultiple()
    {
        return array('pidm' => ['00000000', '00000001', '00000002']);
    }

    /**
     *  Invalid PIDM (Multiple)
     */

    public function testInvalidPIDMMultiple()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsInvalidPIDMMultiple')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());


        try {
            $this->response = $this->job->getJobs();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Invalid pidm.",
                $e->getMessage());
        }

    }

    /**
     * Mock Options for Invalid PIDM Single Test
     */
    public function mockOptionsInvalidPIDMMultiple()
    {
        return array('pidm' => ['00000000', 'test2', '00000002']);
    }

    /**
     *  SQL Injection PIDM (Multiple)
     */

    public function testSQLInjectionPIDMMultiple()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsSQLInjectionPIDMMultiple')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());


        try {
            $this->response = $this->job->getJobs();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Invalid pidm.",
                $e->getMessage());
        }

    }

    /**
     * Mock Options for SQL Injection PIDM Multiple Test
     */
    public function mockOptionsSQLInjectionPIDMMultiple()
    {
        return array('pidm' => ['00000000', '\';--', '00000002']);
    }

    /**************************************************************/
    /*************Standardized Division Code Tests****************/
    /************************************************************/

    /********** Standardized Division Code Single Tests *********/

    /**
     *  Test to get Jobs by Standardized Division Code (Single)
     */

    public function testJobByStandardizedDivisionCode()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsStandardizedDivisionCode')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->expects($this->at(0))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQuerySingle'
            )));

        $this->dbh->expects($this->at(1))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryTotalSingle'
            )));

        $this->response = $this->job->getJobs();
        $payload = $this->response->getPayload();

        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $this->response->getStatus());
        $this->assertEquals($payload, $this->returnJobSingle());

    }

    /**
     * Mock Options for Standardized Division Code Single Job Test
     */
    public function mockOptionsStandardizedDivisionCode()
    {
        return array('standardizedDivisionCode' => ['FSB']);
    }

    /**
     *  Invalid Standardized Division Code (Single)
     */

    public function testInvalidStandardizedDivisionCode()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsInvalidStandardizedDivisionCode')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());


        try {
            $this->response = $this->job->getJobs();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Invalid Standardized Division Code.",
                $e->getMessage());
        }

    }

    /**
     * Mock Options for Invalid Standaridzed Division Code Single Test
     */
    public function mockOptionsInvalidStandardizedDivisionCode()
    {
        return array('standardizedDivisionCode' => ['.asdkna.']);
    }

    /**
     *  SQL Injection Standardized Divsion (Single)
     */

    public function testSQLInjectionStandardizedDivisionCode()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsSQLInjectionStandardizedDivisionCode')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());


        try {
            $this->response = $this->job->getJobs();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Invalid Standardized Division Code.",
                $e->getMessage());
        }

    }

    /**
     * Mock Options for SQL Injection Standardized Division Single Test
     */
    public function mockOptionsSQLInjectionStandardizedDivisionCode()
    {
        return array('standardizedDivisionCode' => ['\';--']);
    }

    /********** Standardized Division Code Multiple Tests *********/

    /**
     *  Test to get Jobs by Standardized Division Code (Multiple)
     */

    public function testJobByStandardizedDivisionCodeMultiple()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsStandardizedDivisionCodeMultiple')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->expects($this->at(0))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryMultiple'
            )));

        $this->dbh->expects($this->at(1))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryTotalMultiple'
            )));

        $this->response = $this->job->getJobs();
        $payload = $this->response->getPayload();

        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $this->response->getStatus());
        $this->assertEquals($payload, $this->returnJobMultiple());

    }

    /**
     * Mock Options for Standardized Division Code Multiple Job Test
     */
    public function mockOptionsStandardizedDivisionCodeMultiple()
    {
        return array('standardizedDivisionCode' => ['FSB', 'CAS', 'PHP']);
    }


    /**
     *  Invalid Division (Multiple)
     */

    public function testInvalidStandardizedDivisionCodeMultiple()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsInvalidStandardizedDivisionCodeMultiple')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());


        try {
            $this->response = $this->job->getJobs();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Invalid Standardized Division Code.",
                $e->getMessage());
        }

    }

    /**
     * Mock Options for Invalid Standardized Division Code Single Test
     */
    public function mockOptionsInvalidStandardizedDivisionCodeMultiple()
    {
        return array('standardizedDivisionCode' => ['FSB', '.asd.a', 'PHP']);
    }

    /**
     *  SQL Injection Standardized Division Code (Multiple)
     */

    public function testSQLInjectionStandardizedDivisionCodeMultiple()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsSQLInjectionStandardizedDivisionCodeMultiple')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());


        try {
            $this->response = $this->job->getJobs();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Invalid Standardized Division Code.",
                $e->getMessage());
        }

    }

    /**
     * Mock Options for SQL Injection Standardized Division Code Multiple Test
     */
    public function mockOptionsSQLInjectionStandardizedDivisionCodeMultiple()
    {
        return array('standardizedDivisionCode' => ['FSB', '\';--', 'PHP']);
    }

    /**************************************************************/
    /*************Standardized Department Code Tests****************/
    /************************************************************/

    /********** Standardized Department Code Single Tests *********/

    /**
     *  Test to get Jobs by Standardized Department Code (Single)
     */

    public function testJobByStandardizedDepartmentCode()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsStandardizedDepartmentCode')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->expects($this->at(0))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQuerySingle'
            )));

        $this->dbh->expects($this->at(1))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryTotalSingle'
            )));

        $this->response = $this->job->getJobs();
        $payload = $this->response->getPayload();

        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $this->response->getStatus());
        $this->assertEquals($payload, $this->returnJobSingle());

    }

    /**
     * Mock Options for Standardized Department Code Single Job Test
     */
    public function mockOptionsStandardizedDepartmentCode()
    {
        return array('standardizedDepartmentCode' => ['CSE']);
    }

    /**
     *  Invalid Standardized Department Code (Single)
     */

    public function testInvalidStandardizedDepartmentCode()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsInvalidStandardizedDepartmentCode')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());


        try {
            $this->response = $this->job->getJobs();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Invalid Standardized Department Code.",
                $e->getMessage());
        }

    }

    /**
     * Mock Options for Invalid Standardized Department Code Single Test
     */
    public function mockOptionsInvalidStandardizedDepartmentCode()
    {
        return array('standardizedDepartmentCode' => ['.asdkna.']);
    }

    /**
     *  SQL Injection Standardized Department (Single)
     */

    public function testSQLInjectionStandardizedDepartmentCode()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsSQLInjectionStandardizedDepartmentCode')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());


        try {
            $this->response = $this->job->getJobs();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Invalid Standardized Department Code.",
                $e->getMessage());
        }

    }

    /**
     * Mock Options for SQL Injection Standardized Department Single Test
     */
    public function mockOptionsSQLInjectionStandardizedDepartmentCode()
    {
        return array('standardizedDepartmentCode' => ['\';--']);
    }

    /********** Standardized Department Code Multiple Tests *********/

    /**
     *  Test to get Jobs by Standardized Department Code (Multiple)
     */

    public function testJobByStandardizedDepartmentCodeMultiple()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsStandardizedDepartmentCodeMultiple')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->expects($this->at(0))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryMultiple'
            )));

        $this->dbh->expects($this->at(1))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryTotalMultiple'
            )));

        $this->response = $this->job->getJobs();
        $payload = $this->response->getPayload();

        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $this->response->getStatus());
        $this->assertEquals($payload, $this->returnJobMultiple());

    }

    /**
     * Mock Options for Standardized Department Code Multiple Job Test
     */
    public function mockOptionsStandardizedDepartmentCodeMultiple()
    {
        return array('standardizedDepartmentCode' => ['CSE', 'EDL', 'ENG']);
    }


    /**
     *  Invalid Department (Multiple)
     */

    public function testInvalidStandardizedDepartmentCodeMultiple()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsInvalidStandardizedDepartmentCodeMultiple')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());


        try {
            $this->response = $this->job->getJobs();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Invalid Standardized Department Code.",
                $e->getMessage());
        }

    }

    /**
     * Mock Options for Invalid Standardized Department Code Single Test
     */
    public function mockOptionsInvalidStandardizedDepartmentCodeMultiple()
    {
        return array('standardizedDepartmentCode' => ['CSE', '.asd.a', 'ENG']);
    }

    /**
     *  SQL Injection Standardized Department Code (Multiple)
     */

    public function testSQLInjectionStandardizedDepartmentCodeMultiple()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsSQLInjectionStandardizedDepartmentCodeMultiple')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());


        try {
            $this->response = $this->job->getJobs();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Invalid Standardized Department Code.",
                $e->getMessage());
        }

    }

    /**
     * Mock Options for SQL Injection Standardized Department Code Multiple Test
     */
    public function mockOptionsSQLInjectionStandardizedDepartmentCodeMultiple()
    {
        return array('standardizedDepartmentCode' => ['CSE', '\';--', 'ENG']);
    }
}
