<?php

namespace RESTng\Service\ERP\Holds;
const OCI_B_CURSOR = 16;
const DB_EMPTY_SET = -1;

class dbOciMockData {
  static private $mockData = array();

  static public function reset() {
    self::$mockData = array();

  }

  static public function set($key, $value) {
    self::$mockData[$key] = $value;

    // Expect arrays for some mock data. Make a copy in our local scope
    // so that function mocks can consume them in a loop.
    switch ($key) {
      case 'mockAttributeList':
        if (!is_array($value)) {
          throw new \Exception('Mock value for attributeList must be an array');
        }
        self::$attributeList = $value;
        break;

    }

  }

  static public function keyExists($key) {
    return array_key_exists($key, self::$mockData);
  }

  static public function get($key) {
    if (!isset(self::$mockData[$key])) {
      throw new \Exception('Mock data for ' . $key . ' not found');
    }

    return self::$mockData[$key];
  }
  static public function getNextValue($key) {
    if (!isset(self::$mockData[$key])) {
      throw new \Exception('Mock data for ' . $key . ' not found');
    }
    if (!is_array(self::$mockData[$key])) {
      throw new \Exception('Mock data for ' . $key . ' must be an array');
    }
    return array_shift(self::$mockData[$key]);
  }
}

/*
  These functions support classes in different namespaces, but we
  don't want to repeat all of the mocking. Simply call the higher
  level mocks and return the value.
*/
function oci_new_cursor($db) {
  return true;
}


function oci_execute($db) {
  return true;
}
$fetchCount = 0;
function oci_fetch_assoc($statement) {
  // oci_fetch_assoc
  dbOciMockData::set('fetchStatement', $statement);
  $result = dbOciMockData::getNextValue('mockfetchAssocResult');
return $result;

}
