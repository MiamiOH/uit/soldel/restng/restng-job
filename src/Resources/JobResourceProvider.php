<?php

namespace MiamiOH\RestngJob\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;

class JobResourceProvider extends ResourceProvider
{


    public function registerDefinitions(): void
    {
        $this->addTag(array(
            'name' => 'job',
            'description' => 'Job resource'
        ));


        $this->addDefinition(array(
            'name' => 'Job.Record',
            'type' => 'object',
            'properties' => array(
                'uniqueId' => array('type' => 'string', 'description' => 'uniqueID for the Person'),
                'pidm' => array('type' => 'string', 'description' => 'pidm for Person the'),
                'plusNumber' => array('type' => 'string', 'description' => 'plus number for the Person'),
                'positionId' => array('type' => 'string', 'description' => 'position num for the job'),
                'primary' => array('type' => 'boolean', 'description' => 'is it a primary job? True/False'),
                'contractType' => array('type' => 'string', 'description' => 'contract type for the job'),
                'title' => array('type' => 'string', 'description' => 'job title'),
                'startDate' => array('type' => 'string', 'description' => 'Job starting date'),
                'endDate' => array('type' => 'string', 'description' => 'Job ending date'),
                'status' => array('type' => 'string', 'description' => 'Job status'),
                'organizationCode' => array('type' => 'string', 'description' => 'Job - Department Code'),
                'organizationDescription' => array('type' => 'string', 'description' => 'Job - Department description'),
                'standardizedDepartmentCode' => array(
                    'type' => 'string',
                    'description' => 'Job - standardized Department Code from BI'
                ),
                'standardizedDepartmentName' => array(
                    'type' => 'string',
                    'description' => 'Job - standardized Department Name from BI'
                ),
                'standardizedDivisionCode' => array(
                    'type' => 'string',
                    'description' => 'Job - standardized Division Code from BI'
                ),
                'standardizedDivisionName' => array(
                    'type' => 'string',
                    'description' => 'Job - standardized Division Name from BI)'
                ),
                'classificationCode' => array(
                    'type' => 'string',
                    'description' => 'Job Classification code from banner : FF'
                ),
                'classificationDescription' => array(
                    'type' => 'string',
                    'description' => 'Job Classification description in banner : Full time faculty'
                ),
                'instructor' => array('type' => 'boolean', 'description' => 'instructor or not. True/False'),
                'contractStatus' => array(
                    'type' => 'string',
                    'description' => 'contract status of the job, especially for librarians. e.g. continuing contract | annual appointment |etc.'
                ),

                'person' => array(
                    'type' => 'object',
                    '$ref' => '#/definitions/Person.Record.Response',
                ),
                'employee' => array(
                    'type' => 'object',
                    '$ref' => '#/definitions/Employee.Record',
                ),

            ),
        ));

        $this->addDefinition([
            'name' => 'job.post.put.model',
            'type' => 'object',
            'properties' => [
                'muid' => [
                    'type' => 'string',
                    'enum' => ['required|string']
                ],
                'positionNumber' => [
                    'type' => 'string',
                    'enum' => ['required|string']
                ],
                'positionSuffix' => [
                    'type' => 'string',
                    'enum' => ['required|string']
                ],
                'effectiveDate' => [
                    'type' => 'string',
                    'enum' => ['required|date']
                ],
                'personnelChangeDate' => [
                    'type' => 'string',
                    'enum' => ['required|date']
                ],
                'status' => [
                    'type' => 'string',
                    'enum' => ['required|string']
                ],
                'employeeClassificationCode' => [
                    'type' => 'string',
                    'enum' => ['required|string']
                ],
                'payIdCode' => [
                    'type' => 'string',
                    'enum' => ['required|string']
                ],
                'timeSheetOrganization' => [
                    'type' => 'string',
                    'enum' => ['required|string']
                ],
                'appointmentPercentage' => [
                    'type' => 'string',
                    'enum' => ['required|string']
                ],
                'hoursPerDay' => [
                    'type' => 'string',
                    'enum' => ['required|string']
                ],
                'hoursPerPay' => [
                    'type' => 'string',
                    'enum' => ['required|string']
                ],
                'shift' => [
                    'type' => 'string',
                    'enum' => ['required|string']
                ],
                'assignmentSalary' => [
                    'type' => 'string',
                    'enum' => ['required|string']
                ],
                'factor' => [
                    'type' => 'string',
                    'enum' => ['required|string']
                ],
                'annualSalary' => [
                    'type' => 'string',
                    'enum' => ['required|string']
                ],
                'perPaySalary' => [
                    'type' => 'string',
                    'enum' => ['required|string']
                ],
                'pays' => [
                    'type' => 'string',
                    'enum' => ['required|string']
                ],
                'perPayDeferredAmount' => [
                    'type' => 'string',
                    'enum' => ['required|string']
                ],
                'salaryGroupCode' => [
                    'type' => 'string',
                    'enum' => ['required|string']
                ],
                'timeEntryMethod' => [
                    'type' => 'string',
                    'enum' => ['required|string']
                ],
                'timeEntryType' => [
                    'type' => 'string',
                    'enum' => ['required|string']
                ],
                'timeInOutIndicator' => [
                    'type' => 'string',
                    'enum' => ['required|string']
                ],
                'leaveReportMethod' => [
                    'type' => 'string',
                    'enum' => ['required|string']
                ],
                'leaveReportPayId' => [
                    'type' => 'string',
                    'enum' => ['required|string']
                ],
                'description' => [
                    'type' => 'string'
                ],
                'fullTimeEquivalency' => [
                    'type' => 'string',
                ],
                'regularRate' => [
                    'type' => 'string',
                ],
                'changeReasonCode' => [
                    'type' => 'string',
                ],
                'salaryTable' => [
                    'type' => 'string',
                ],
                'salaryGrade' => [
                    'type' => 'string',
                ],
                'salaryStep' => [
                    'type' => 'string',
                ],
                'employerCode' => [
                    'type' => 'string',
                ],
                'longevityCode' => [
                    'type' => 'string',
                ],
                'supervisorMuid' => [
                    'type' => 'string',
                ],
                'supervisorPosition' => [
                    'type' => 'string',
                ],
                'supervisorSuffix' => [
                    'type' => 'string',
                ],
                'premiumPayCode' => [
                    'type' => 'string',
                ],
                'timeSheetChartOfAccountsCode' => [
                    'type' => 'string',
                ],
            ]
        ]);

        $this->addDefinition(array(
            'name' => 'Job.Record.Collection',
            'type' => 'array',
            'items' => [
                '$ref' => '#/definitions/Job.Record'
            ]
        ));
    }

    public function registerServices(): void
    {
        $this->addService([
            'name' => 'Job',
            'class' => 'MiamiOH\RestngJob\Services\Job',
            'description' => 'Provide CRUD interface to Job model',
            'set' => [
                'database' => ['type' => 'service', 'name' => 'APIDatabaseFactory'],
                'configuration' => ['type' => 'service', 'name' => 'APIConfiguration']
            ]
        ]);
    }

    public function registerResources(): void
    {
        $this->addResource(array(
            'action' => 'read',
            'name' => 'job.v1',
            'description' => 'Get collection of job data from Banner.',
            'pattern' => '/job/v1',
            'service' => 'Job',
            'method' => 'getJobs',
            'returnType' => 'collection',
            'isPartialable' => true,
            'isPageable' => true,
            'defaultPageLimit' => 20,
            'maxPageLimit' => 500,
            'tags' => array('job'),
            'options' => array(
                'uniqueId' => array('type' => 'list', 'description' => 'List of uniqueIDs to query for'),
                'pidm' => array('type' => 'list', 'description' => 'List of pidms to query for'),
                'positionId' => array('description' => 'Enter position number to get the job information for that position'),
                'suffix' => array('description' => 'Enter position suffix to get the job information for that position'),
                'instructor' => array(
                    'description' => 'If set to true, return only instructor/teaching jobs.',
                    'enum' => ['true', 'false']
                ),
                'primary' => array(
                    'description' => 'If set to true, return only primary jobs only.',
                    'enum' => ['true', 'false']
                ),
                'includeSupplement' => array('description' => 'True or False. True: include supplement/stipend/consultant/special-assignment types of jobs. False: do not include those jobs. Default: false.'),
                'includeOverload' => array('description' => 'True or False. True: include jobs that has a contract type of O (Overload). False: do not include those jobs. Default: false.'),
                'standardizedDivisionCode' => array(
                    'type' => 'list',
                    'description' => 'CAS, FBS, CCA, EHS, CEC , CPS. Comma-separated division code list.'
                ),
                'standardizedDepartmentCode' => array(
                    'type' => 'list',
                    'description' => 'CSE, MCS etc. Comma-separated department code list.'
                ),
                'traditionalDepartment' => array(
                    'description' => 'Return traditional(Oxford) department code and names as the standardizedDepartmentCode and standardizedDepartmentName. Default: false.',
                    'enum' => ['true', 'false']
                ),
            ),
            'middleware' => array(
                'authenticate' => array(),
                'authorize' => array(
                    array(
                        'type' => 'authMan',
                        'application' => 'WebServices',
                        'module' => 'Job',
                        'key' => 'view'
                    ),

                ),
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'A collection of classes',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/Job.Record.Collection',
                    )
                ),
            )
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'job.v1.muid',
            'description' => 'Get the job information for the given uniqueID.',
            'pattern' => '/job/v1/:muid',
            'service' => 'Job',
            'method' => 'getJobsforID',
            'returnType' => 'collection',
            'tags' => array('job'),
            'isPartialable' => true,
            'params' => array(
                "muid" => array(
                    'description' => "A Person's uniqueID or pidm",
                    'alternateKeys' => ['uniqueID', 'pidm']
                ),
            ),
            'options' => array(
                'includeSupplement' => array('description' => 'True or False. True: include supplement/stipend/consultant/special-assignment types of jobs. False: do not include those jobs. Default: false.'),
                'includeOverload' => array('description' => 'True or False. True: include jobs that has a contract type of O (Overload). False: do not include those jobs. Default: false.'),
                'traditionalDepartment' => array(
                    'description' => 'Return traditional(Oxford) department code and names as the standardizedDepartmentCode and standardizedDepartmentName. Default: false.',
                    'enum' => ['true', 'false']
                ),
            ),
            'middleware' => array(
                'authenticate' => array(),
                'authorize' => array(
                    array(
                        'type' => 'authMan',
                        'application' => 'WebServices',
                        'module' => 'Job',
                        'key' => 'view'
                    ),
                    array(
                        'type' => 'self',
                        'param' => 'muid',
                    ),

                ),
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'A class object',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/Job.Record.Collection',
                    )
                ),
                App::API_NOTFOUND => array(
                    'description' => 'Job info not found'
                )
            )
        ));

        $this->addResource([
            'action' => 'create',
            'name' => 'job.post.single.v1',
            'description' => 'Create a job record',
            'pattern' => '/job/v1',
            'service' => 'Job',
            'method' => 'postSingle',
            'returnType' => 'collection',
            'tags' => ['job'],
            'body' => [
                'required' => true,
                'description' => 'job data',
                'schema' => [
                    '$ref' => '#/definitions/job.post.put.model'
                ]
            ],
            'middleware' => [
                'authenticate' => ['type' => 'token'],
                'authorize' => [
                    'application' => 'WebServices',
                    'module' => 'Job',
                    'key' => ['create', 'all']
                ],

            ],
            'responses' => [
                App::API_CREATED => [
                    'description' => 'Creates successfully.',
                ],
                App::API_BADREQUEST => [
                    'description' => 'Some or all data are bad.',
                ],
                App::API_FAILED => [
                    'description' => 'Insert operation failed.',
                ],
                App::API_UNAUTHORIZED => [
                    'description' => 'Unauthorized access.',
                ],
            ],
        ]);

        $this->addResource([
            'action' => 'update',
            'name' => 'job.put.single.v1',
            'description' => 'Update a job record',
            'pattern' => '/job/v1',
            'service' => 'Job',
            'method' => 'putSingle',
            'returnType' => 'collection',
            'tags' => ['job'],
            'body' => [
                'required' => true,
                'description' => 'job data',
                'schema' => [
                    '$ref' => '#/definitions/job.post.put.model'
                ]
            ],
            'middleware' => [
                'authenticate' => ['type' => 'token'],
                'authorize' => [
                    'application' => 'WebServices',
                    'module' => 'Job',
                    'key' => ['update', 'edit', 'all']
                ],

            ],
            'responses' => [
                App::API_OK => [
                    'description' => 'Update successfully.',
                ],
                App::API_BADREQUEST => [
                    'description' => 'Some or all data are bad.',
                ],
                App::API_FAILED => [
                    'description' => 'Update operation failed.',
                ],
                App::API_UNAUTHORIZED => [
                    'description' => 'Unauthorized access.',
                ],
            ],
        ]);
    }

    public function registerOrmConnections(): void
    {

    }
}
