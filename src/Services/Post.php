<?php
/**
 * Created by PhpStorm.
 * User: q
 * Date: 12/4/18
 * Time: 4:40 PM
 */

namespace MiamiOH\RestngJob\Services;

use MiamiOH\RESTng\Util\User;
use MiamiOH\RESTng\Util\Response;
use MiamiOH\RESTng\Util\Request;
use MiamiOH\RestngJob\Repositories\JobRepository;
use MiamiOH\RestngJob\Objects\Job;

class Post
{
    /**
     * @param Request $request
     * @param Response $response
     * @param User $user
     * @param JobRepository $repository
     * @return Response
     * @throws \Exception
     */
    public function postSingle(
        Request $request,
        Response $response,
        User $user,
        JobRepository $repository
    ): Response
    {
        $status = \MiamiOH\RESTng\App::API_CREATED;

        $data = $request->getData();

        if (empty($data)) {
            $payload['errors'][] = 'No Data';
            $status = \MiamiOH\RESTng\App::API_BADREQUEST;

            $response->setPayload($payload);
            $response->setStatus($status);
            return $response;
        }

        $data['userId'] = $user->getUsername();
        $data['dataOrigin'] = 'WebService';

        try {
            $job = Job::fromArray($data);

            $result = $repository->create($job);
        } catch (\Exception $e) {
            $response->setPayload([$e->getMessage()]);
            $response->setStatus(\MiamiOH\RESTng\App::API_FAILED);
            return $response;
        }

        // DONE
        $response->setPayload(['1 record created.']);
        $response->setStatus($status);
        return $response;
    }
}
