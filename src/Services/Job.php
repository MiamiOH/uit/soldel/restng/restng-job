<?php
/*
---------------------------------------------------------------------------------------------------------------
FILE NAME: Job.class.php

Copyright (c) 2015 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Meenakshi Kandasamy

DESCRIPTION:  The Job Service is designed to get job records.

ENVIRONMENT DEPENDENCIES: RESTNG FRAMEWORK

AUDIT TRAIL:

DATE    PRJ-TSK          UniqueID
Description:

06/XX/2016              kandasm
Description:  Initial Program

08/23/2016              patelah
Description:  Added the field to include Division Code and comments wherever changes are made.

08/24/2016              patelah
Description:  Added the filter for department code and division code.

08/30/2016              patelah
Description:  Added the query to include division name.

09/07/2016              schmidee
Description:  Corrected issues with the Division, Department, and Position Number Filters.

10/19/2016              gengx
Description:  Removed the professionalLibrarian field

10/25/2016              gengx
Description:  Split the filter 'includeSupplement' into two filters 'incudeSupplement' and 'includeOverload'

10/26/2016              gengx
Description:  Rename 'standardizedDeptDescription' as 'standardizedDepartmentName'

11/10/2016              gengx
Description:  Added a filter 'traditionalDepartment' to switch the stardardized department fields between new/old department.
---------------------------------------------------------------------------------------------------------------
*/

namespace MiamiOH\RestngJob\Services;

use MiamiOH\RESTng\Legacy\DataSource;
use MiamiOH\RESTngIlluminateIntegration\RESTngEloquentFactory;
use MiamiOH\RestngJob\Repositories\JobRepository;
use MiamiOH\RestngJob\Repositories\JobRepositorySQL;
use MiamiOH\RestngJob\Repositories\MUIDRepositoryResourceCall;

class Job extends \MiamiOH\RESTng\Service
{
    private $datasource = 'MUWS_SEC_PROD';
    private $dbh;
    private $config;

    /**
     * @var JobRepository
     */
    private $repository = null;

    public function setDatabase($database)
    {
        $this->dbh = $database->getHandle($this->datasource);
    }

    public function getDependencies()
    {
        date_default_timezone_set('US/Eastern');

        $muidCall = function (array $params = [], array $options = []) {
            return $this->callResource('muid.get', $params, $options);
        };

        $muidRepository = new MUIDRepositoryResourceCall($muidCall);

        $this->repository = new JobRepositorySQL($muidRepository);
    }

    /**
     * @return \MiamiOH\RESTng\Util\Response
     * @throws \Exception
     */
    public function postSingle()
    {
        $this->getDependencies();

        $postService = new Post();

        $response = $postService->postSingle(
            $this->getRequest(),
            $this->getResponse(),
            $this->getApiUser(),
            $this->repository
        );

        return $response;
    }

    /**
     * @return \MiamiOH\RESTng\Util\Response
     * @throws \Exception
     */
    public function putSingle()
    {
        $this->getDependencies();

        $putService = new Put();

        $response = $putService->putSingle(
            $this->getRequest(),
            $this->getResponse(),
            $this->getApiUser(),
            $this->repository
        );

        return $response;
    }

    //Setter injection for configuration from RESTNG framework.

    public function setConfiguration($configuration)
    {
        $this->configuration = $configuration;
        $this->config = $this->configuration->getConfiguration('RESTng-Job', 'Internal Configs');
    }

    //Get pidms for the given uniqueIds

    protected function getPidms($uniqueIds)
    {
        if (!$this->dbh) {
            $this->dbh = $this->database->getHandle($this->datasource_name);
        }
        //$uniqueIds=array($uniqueIds);
        $uniqueIds = array_map(function ($val) {
            return strtoupper(trim($val));
        }, $uniqueIds);

        $records = $this->dbh->queryall_array("SELECT szbuniq_pidm FROM szbuniq WHERE szbuniq_unique_id IN (?" . str_repeat(",?",
                count($uniqueIds) - 1) . ")", $uniqueIds);
        $pidms = array_column($records, 'szbuniq_pidm');
        return $pidms;
    }

    protected function getPidm($uniqueId)
    {
        $pidm = $this->dbh->queryfirstcolumn('
	      select szbuniq_pidm from szbuniq where upper(szbuniq_unique_id) = upper(?)
	      ', $uniqueId);

        return $pidm;
    }


    // Build "in" clause for SQL queries

    protected function buildInString($array, $isString)
    {
        $result = "";
        foreach ($array as $element) {
            if ($isString) {
                $result = $result . ",'" . strtoupper($element) . "'";
            } else {
                $result = $result . "," . strtoupper($element);
            }
        }
        $result = substr($result, 1);
        return $result;

    }


    public function getJobs()
    {

        $request = $this->getRequest();
        $response = $this->getResponse();

        $options = $request->getOptions();

        //enforce paging

        $offset = $request->getOffset();
        $limit = $request->getLimit();


        $where = array();
        $values = array();

        $where[] = '1=1';
        #$values ='';
        if (isset($options['pidm'])) {
            $this->validateInput($options['pidm'], $this->getPattern("pidm"), "Invalid pidm.");
            $pidms = implode(',', $options['pidm']);
            $where[] = "nbrjobs_pidm IN ($pidms)";
        }

        if (isset($options['standardizedDivisionCode']) && !is_array($options['standardizedDivisionCode'])) {
            $options['standardizedDivisionCode'] = array($options['standardizedDivisionCode']);
        }

        // Division Code Filter
        if (isset($options['standardizedDivisionCode'])) {
            $divCodes = array_unique(array_map('strtoupper', $options['standardizedDivisionCode']));

            $this->validateInput($divCodes, '/^[A-Za-z0-9]{1,}$/', "Invalid Standardized Division Code.");
            $where[] = "(cleanDiv.STANDARDIZED_DIVISION_CD in (" . $this->buildInString($divCodes, true) . "))";
        }

        if (isset($options['standardizedDepartmentCode']) && !is_array($options['standardizedDepartmentCode'])) {
            $options['standardizedDepartmentCode'] = array($options['standardizedDepartmentCode']);
        }

        //Department Code Filter
        if (isset($options['standardizedDepartmentCode'])) {
            $deptCodes = array_unique(array_map('strtoupper', $options['standardizedDepartmentCode']));
            $this->validateInput($deptCodes, '/^[A-Za-z0-9]{1,}$/', 'Invalid Standardized Department Code.');
            $where[] = "(cleanDept.STANDARDIZED_DEPARTMENT_CD in (" . $this->buildInString($deptCodes, true) . "))";

        }

        if (isset($options['uniqueId'])) {
            $this->validateInput($options['uniqueId'], $this->getPattern("uniqueId"), "Invalid Unique ID.");
            $uniqueids = array_unique(array_map('strtoupper', $options['uniqueId']));
            $where[] = '(szbuniq_unique_id in (' . $this->buildInString($uniqueids, true) . "))";
        }

        if (isset($options['instructor']) && $options['instructor'] === 'true') {
            #$is_instructor =$options['instructor'];
            #echo var_export($is_instructor,true);
            #print_r( !$is_instructor ? 'false' : '');
            // if($options['instructor'] === 'true'){
            $where[] = "nbrjobs_ecls_code like ('F%')";
            // }

        }

        if (isset($options['primary']) && $options['primary'] === 'true') {
            $where[] = "nbrbjob_contract_type = 'P'";
        }

        if (isset($options['positionId'])) {
            $posn = $options['positionId'];
            $this->validateInput($posn, '/^[0-9]+$/', "Invalid Position ID.");
            $where[] = "nbrjobs_posn = $posn";

            if (isset($options['suffix'])) {
                $suffix = $options['suffix'];
                $this->validateInput($suffix, '/^[0-9]{2}$/', "Invalid Position Suffix.");
                $where[] = "nbrjobs_suff = $suffix";
            }
        }


        if (isset($options['includeSupplement'])) {
            $supplement = $options['includeSupplement'];
            $this->validateInput($supplement, '/^(TRUE|FALSE)$/i', "Invalid includeSupplement filter value.");
            if (strtoupper($supplement) === 'FALSE') {
                $where = array_merge($where, $this->getTitleFilters());
            }
        } else {
            //default to FALSE
            $where = array_merge($where, $this->getTitleFilters());
        }


        if (isset($options['includeOverload'])) {
            $overload = $options['includeOverload'];
            $this->validateInput($overload, '/^(TRUE|FALSE)$/i', "Invalid includeOverload filter value.");
            if (strtoupper($overload) === 'FALSE') {
                $where[] = "nbrbjob_contract_type != 'O'";
            }
        } else {
            //default to FALSE
            $where[] = "nbrbjob_contract_type != 'O'";
        }

        //set the default filter value for standardized department code and name
        $deptWhere = "cleanDept.CAMPUS_GROUP_CD = case when f_orgn_hier2_fnc('C',a.nbrjobs_orgn_code_ts,2) = 'Hamilton Campus' then 'H'
                         when f_orgn_hier2_fnc('C',a.nbrjobs_orgn_code_ts,2) = 'Middletown Campus' then 'M'
                        else 'O' end";
        //change the filter value if a valide option value of TRUE is passed in
        if (isset($options['traditionalDepartment'])) {
            $traditionalDepartment = $options['traditionalDepartment'];
            $this->validateInput($traditionalDepartment, '/^(TRUE|FALSE)$/i',
                "Invalid traditionalDepartment filter value.");
            if (strtoupper($traditionalDepartment) === 'TRUE') {
                $deptWhere = "cleanDept.CAMPUS_GROUP_CD='O'";
            }
        }
        $where[] = $deptWhere;


        // $titleFilterWhere = $this->getTitleFilters();
        $subObjects = $request->getSubObjects();


        $query = "
        SELECT *
        FROM (SELECT c.*, ROWNUM rnum
            FROM ("
            . $this->getJobsQuery(join(' AND ', $where)) .
            " order by a.nbrjobs_pidm) c
          WHERE  rownum <=" . ($limit + $offset - 1) . ")
        WHERE  rnum >= " . $offset;

        // echo $query;
        // exit;

        $results = $this->dbh->queryall_array($query);
        //print_r($results);
        $pidmList = array();

        $rows = [];
        if (is_array($results) || is_object($results)) {
            foreach ($results as $row) {
                $pidmList[] = $row['pidm'];
                unset($row['rnum']);
                $rows[] = $this->camelCaseKeys($row);
            }

        }
        //get the total object number

        $query = "SELECT count(a.nbrjobs_pidm) " . $this->getQueryBody() . " AND " . join(' AND ', $where);

        $totalObjects = $this->dbh->queryfirstcolumn($query);


        //compositing other resources
        if (count($rows) > 0) {
            $results = $this->addSubObjects($rows, $subObjects, false, $limit);
        } else {
            $results = $rows;
        }

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($results);
        $response->setTotalObjects($totalObjects);
        return $response;
    }

    public function getJobsforID()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();
        $options = $request->getOptions();

        $uIdValue = $request->getResourceParam('muid');
        $keyField = 'uniqueId';
        switch ($request->getResourceParamKey('muid')) {
            case 'uniqueId':
                $keyField = 'uniqueId';
                break;
            case 'pidm':
                $keyField = 'pidm';
                break;
        }

        $subObjects = $request->getSubObjects();

        //make sure uid exists
        if ($uIdValue === null) {
            $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
            return $response;
        }
        if ($keyField === 'uniqueId') {
            $pidm = $this->getPidm($uIdValue);

        } else {
            $pidm = $uIdValue;
        }


        if ($pidm === DB_EMPTY_SET) {
            throw new \Exception('Invalid Unique ID provided.');
        }
        //filters
        $where[] = "a.nbrjobs_pidm = $pidm";

        //filter: whether to include supplemental types of jobs; defaults to false
        if (isset($options['includeSupplement'])) {
            $supplement = $options['includeSupplement'];
            $this->validateInput($supplement, '/^(TRUE|FALSE)$/i', "Invalid includeSupplement filter value.");
            if (strtoupper($supplement) === 'FALSE') {
                $where = array_merge($where, $this->getTitleFilters());
            }
        } else {
            //default to FALSE
            $where = array_merge($where, $this->getTitleFilters());
        }

        //filter: whether to include overload type of jobs; defaults to false
        if (isset($options['includeOverload'])) {
            $overload = $options['includeOverload'];
            $this->validateInput($overload, '/^(TRUE|FALSE)$/i', "Invalid includeOverload filter value.");
            if (strtoupper($overload) === 'FALSE') {
                $where[] = "nbrbjob_contract_type != 'O'";
            }
        } else {
            //default to FALSE
            $where[] = "nbrbjob_contract_type != 'O'";
        }

        //set the default filter value for standardized department code and name
        $deptWhere = "cleanDept.CAMPUS_GROUP_CD = case when f_orgn_hier2_fnc('C',a.nbrjobs_orgn_code_ts,2) = 'Hamilton Campus' then 'H'
                         when f_orgn_hier2_fnc('C',a.nbrjobs_orgn_code_ts,2) = 'Middletown Campus' then 'M'
                        else 'O' end";
        //change the filter value if a valide option value of TRUE is passed in
        if (isset($options['traditionalDepartment'])) {
            $traditionalDepartment = $options['traditionalDepartment'];
            $this->validateInput($traditionalDepartment, '/^(TRUE|FALSE)$/i',
                "Invalid traditionalDepartment filter value.");
            if (strtoupper($traditionalDepartment) === 'TRUE') {
                $deptWhere = "cleanDept.CAMPUS_GROUP_CD='O'";
            }
        }
        $where[] = $deptWhere;

        // echo $this->getJobsQuery(join(' AND ', $where));
        // exit;
        $results = $this->dbh->queryall_array($this->getJobsQuery(join(' AND ', $where)));

        foreach ($results as $row) {

            if (is_array($subObjects) && array_key_exists('person', $subObjects)) {
                $personResponse = $this->callResource('person.read.alternate',
                    array(
                        'params' => array('personID' => 'pidm=' . $pidm),
                        'fields' => $subObjects['person']
                    ));
                $row['person'] = $this->camelCaseKeys($personResponse->getPayload());
            }
            if (is_array($subObjects) && array_key_exists('employee', $subObjects)) {
                #$employeeResponse = $this->callResource('employee.v1.employeeId',array('params'=>(array('employeeId'=>$uidValue)),'fields' => $subObjects['employee']));
                $getEmpInfo = $this->callResource('employee.v1.muid',
                    array('params' => (array('muid' => 'pidm=' . $pidm))));
                $row['employee'] = $this->camelCaseKeys($getEmpInfo->getPayload());
            }

            $rows[] = $this->camelCaseKeys($row);

        }

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($rows);

        return $response;
    }

    private function getJobsQuery($whereStr)
    {
        $fieldList = $this->getFieldList();

        return "SELECT $fieldList " . $this->getQueryBody() . " AND " . $whereStr;


    }

    private function getQueryBody()
    {
        return "from nbrjobs a,nbrbjob_add,spriden,szbuniq,IA_REF_ORGANIZATION_LEVEL_5,
               (select unique(IA_REF_STANDARDIZE_DIVISION.STANDARDIZED_DIVISION_CD), IA_REF_STANDARDIZE_DIVISION.DIVISION_CD, IA_REF_STANDARDIZE_DIVISION.STANDARDIZED_DIVISION_NM from IA_REF_STANDARDIZE_DIVISION where effective_to_dt >= sysdate and effective_from_dt <= sysdate) cleanDiv,
               (select unique(IA_ref_standardize_department.STANDARDIZED_DEPARTMENT_CD), IA_ref_standardize_department.DEPARTMENT_CD, IA_ref_standardize_department.STANDARDIZED_DEPARTMENT_NM, IA_ref_standardize_department.campus_group_cd from IA_ref_standardize_department where effective_to_dt >= sysdate and effective_from_dt <= sysdate) cleanDept

          WHERE a.nbrjobs_pidm = Nbrbjob_Pidm
          and a.nbrjobs_pidm = spriden_Pidm
          and a.nbrjobs_pidm = szbuniq_Pidm
          and spriden_change_ind is null
          And (Nbrbjob_End_Date Is Null
          Or Nbrbjob_End_Date >= Sysdate)
          And Nbrbjob_Posn = A.Nbrjobs_Posn
          and nbrbjob_suff = a.nbrjobs_suff
          and IA_REF_ORGANIZATION_LEVEL_5.ORGANIZATION_LEVEL_5_CD = a.nbrjobs_orgn_code_ts
          and IA_REF_ORGANIZATION_LEVEL_5.DIVISION_CD = cleanDiv.DIVISION_CD
          and IA_REF_ORGANIZATION_LEVEL_5.DEPARTMENT_CD = cleanDept.DEPARTMENT_CD
          and sysdate between IA_REF_ORGANIZATION_LEVEL_5.EFFECTIVE_FROM_DT and IA_REF_ORGANIZATION_LEVEL_5.EFFECTIVE_TO_DT
          AND a.nbrjobs_effective_date =  (SELECT max(b.nbrjobs_effective_date)
                                                 FROM nbrjobs b
                                                 WHERE a.nbrjobs_pidm = b.nbrjobs_pidm
                                                 AND a.NBRJOBS_POSN = b.NBRJOBS_POSN
                                                 AND a.NBRJOBS_SUFF = b.NBRJOBS_SUFF
                                                 AND b.nbrjobs_effective_date <= sysdate)
                                                 ";
    }

    private function getTitleFilters()
    {

        if (!trim($this->config['title_filters'])) {
            return array();
        }

        $titleFilters = explode(",", trim($this->config['title_filters']));

        $titleFilterWhere = array();
        // $titleFilterWhere[] = "nbrbjob_contract_type != 'O'";

        foreach ($titleFilters as $filter) {
            $titleFilterWhere[] = "upper(a.nbrjobs_desc) not like '%" . strtoupper(trim($filter)) . "%'";

        }
        return $titleFilterWhere;
    }


    private function getFieldList()
    {
        $request = $this->getRequest();
        // print_r($request);
        // exit;
        $fieldList = '';

        $replaceList = array(
            'uniqueId' => "szbuniq_unique_id as unique_Id",
            'pidm' => "nbrjobs_pidm as pidm",
            'plusNumber' => "spriden_id as plus_Number",
            'positionId' => "nbrjobs_posn as position_Id",
            'positionSuffix' => "nbrjobs_suff as position_suffix",
            'primary' => "case when nbrbjob_contract_type = 'P' then 'true' else 'false' END as primary",
            'contractType' => "nbrbjob_contract_type as contract_type",
            // 'title' =>  "nbrjobs_desc title",
            'title' => "nvl(title_1,nbrjobs_desc) as title",
            'payIdCode' => 'nbrjobs_pict_code as payIdCode',
            'timeSheetChartOfAccountsCode' => 'nbrjobs_coas_code_ts as time_sheet_chart_of_accounts_code',
            'timeSheetOrganization' => 'nbrjobs_orgn_code_ts as time_sheet_organization',
            'salaryTable' => 'nbrjobs_sal_table as salary_table',
            'salaryGrade' => 'nbrjobs_sal_grade as salary_grade',
            'salaryStep' => 'nbrjobs_sal_step as salary_step',
            'annualSalary' => 'nbrjobs_ann_salary as annual_salary',
            'perPaySalary' => 'nbrjobs_per_pay_salary as per_pay_salary',
            'appointmentPercentage' => 'nbrjobs_appt_pct as appointment_percentage',
            'fullTimeEquivalency' => 'nbrjobs_fte as full_time_equivalency',
            'hoursPerPay' => 'nbrjobs_hrs_pay as hours_per_pay',
            'hoursPerDay' => 'nbrjobs_hrs_day as hours_per_day',
            'shift' => 'nbrjobs_shift as shift',
            'regularRate' => 'nbrjobs_reg_rate as regular_rate',
            'assignmentSalary' => 'nbrjobs_assgn_salary as assignment_salary',
            'factor' => 'nbrjobs_factor as factor',
            'pays' => 'nbrjobs_pays as pays',
            'perPayDeferredAmount' => 'nbrjobs_per_pay_defer_amt as per_pay_deferred_amount',
            'changeReasonCode' => 'nbrjobs_jcre_code as change_reason_code',
            'salaryGroupCode' => 'nbrjobs_sgrp_code as salary_group_code',
            'employerCode' => 'nbrjobs_empr_code as employer_code',
            'longevityCode' => 'nbrjobs_lgcd_code as logevity_code',
            'startDate' => "to_char(nbrjobs_effective_date,'YYYY-MM-DD') as start_date",
            'endDate' => "to_char(nbrbjob_end_date,'YYYY-MM-DD') as end_date",
            'supervisorPidm' => 'nbrjobs_supervisor_pidm as supervisor_pidm',
            'supervisorName' => "(select spriden_first_name || ' ' || spriden_last_name from spriden where spriden_pidm = nbrjobs_supervisor_pidm and spriden_change_ind is null) as supervisor_name",
            'supervisorPosition' => 'nbrjobs_supervisor_posn as supervisor_position',
            'supervisorSuffix' => 'nbrjobs_supervisor_suff as supervisor_suffix',
            'personnelChangeDate' => 'nbrjobs_pers_chg_date as personnel_change_date',
            'premiumPayCode' => 'nbrjobs_pcat_code as premium_pay_code',
            'timeEntryMethod' => 'nbrjobs_time_entry_method as time_entry_method',
            'timeEntryType' => 'nbrjobs_time_entry_type as time_entry_type',
            'timeInOutIndicator' => 'nbrjobs_time_in_out_ind as time_in_out_indicator',
            'leaveReportMethod' => 'nbrjobs_leav_rept_method as leave_report_method',
            'leaveReportPayId' => 'nbrjobs_pict_code_leav_rept as leave_report_pay_id',
            'status' => "nbrjobs_status as status",
            'organizationCode' => "nbrjobs_orgn_code_ts as organization_Code",
            'organizationDescription' => "nvl(f_orgn_hier2_fnc(a.nbrjobs_COAS_CODE_TS,a.NBRJOBS_ORGN_CODE_TS,5),
                                                                  f_orgn_hier2_fnc(a.nbrjobs_COAS_CODE_TS,a.NBRJOBS_ORGN_CODE_TS,4)) as organizationDescription",
            'classificationCode' => "nbrjobs_ecls_code  as classification_Code",
            'classificationDescription' => "(select ptrecls_long_desc from ptrecls where ptrecls_code = nbrjobs_ecls_code)  as classification_Description",
            // 'standardizedDepartmentCode' => "fz_get_standardized_dept(nbrjobs_orgn_code_ts,'code') as standardized_Department_Code",
            // 'standardizedDepartmentName' => "fz_get_standardized_dept(nbrjobs_orgn_code_ts,'name') as standardized_Department_Name",
            'standardizedDepartmentCode' => "cleanDept.standardized_department_cd as standardized_Department_Code",
            'standardizedDepartmentName' => "cleanDept.standardized_department_nm as standardized_Department_Name",
            'standardizedDivisionCode' => 'cleanDiv.STANDARDIZED_DIVISION_CD as standardized_division_code',
            'standardizedDivisionName' => 'cleanDiv.STANDARDIZED_DIVISION_NM as standardized_division_name',
            'responsibleOffice' => "(select pprccmt_text from pprccmt where pprccmt_cmty_code = 'RO' and pprccmt_pidm = nbrjobs_pidm)  as responsible_office",
            'instructor' => "case when nbrjobs_ecls_code like ('F%') then 'true' else 'false' end  as instructor",
            'contractStatus' => "regexp_substr(status_1,'[^- ].+') as contract_Status",
        );

        if ($request->isPartial()) {
            #$fieldListOriginal = implode(', ', $this->snakeCaseValues($request->getPartialRequestFields()));
            // print_r($request->getPartialRequestFields());
            // exit;
            $fieldListOriginal = implode(', ', $this->camelCaseKeys($request->getPartialRequestFields()));

            //always return pidm as it's used for calling sub objects
            if (strpos($fieldListOriginal, "pidm") === false) {
                $fieldListOriginal = $fieldListOriginal . ",pidm";
            }
        } else {
            $fieldListOriginal = implode(', ', array_keys($replaceList));
        }


        $fieldList = str_replace(array_keys($replaceList), array_values($replaceList), $fieldListOriginal);
        return $fieldList;
    }


    private function getPattern($val)
    {
        if ($val == 'pidm') {
            return '/^\d{1,8}$/';
        } else {
            if ($val == 'uniqueId') {
                return '/^\w{1,8}$/';
            }
        }
    }

    private function validateInput($input, $pattern, $errMesg)
    {
        if (is_array($input)) {
            foreach ($input as $value) {

                if (!preg_match($pattern, $value)) {
                    throw new \MiamiOH\RESTng\Exception\BadRequest($errMesg);
                }

            }
        } else {
            if (!preg_match($pattern, $input)) {
                throw new \MiamiOH\RESTng\Exception\BadRequest($errMesg);
            }
        }
    }


//add the sub objects to the payload
    private function addSubObjects($payload, $subObjects, $singleResource, $pageSize)
    {

        if ($singleResource) {

            $pidm = $payload['pidm'];

            //calling single resource for all sub objects
            foreach (array_keys($subObjects) as $subObj) {
                if ($subObj == "employee") {
                    $employee = $this->callResource(
                        'employee.v1.muid', array('params' => array('muid' => "pidm=" . $pidm))
                    );
                    $payload['employee'] = $employee->getPayload();
                } elseif ($subObj == "person") {
                    $person = $this->callResource(
                        'person.read.alternate', array('params' => array('personID' => "pidm=" . $pidm))
                    );
                    $payload['person'] = $person->getPayload();
                }
            }
        } else {
            $pidms = array();

            foreach ($payload as $job) {
                $pidms[] = $job['pidm'];
            }
            $pidmStr = implode(",", $pidms);


            $returnEmployee = $returnPerson = false;

            foreach (array_keys($subObjects) as $subObj) {
                switch ($subObj) {
                    case 'employee':

                        $returnEmployee = true;
                        $offset = 1;
                        $limit = $pageSize;
                        $employeePidms = array();
                        $employeePayload = array();
                        do {

                            $employeeResponse = $this->callResource(
                                'employee.v1',
                                array('options' => array('pidm' => $pidmStr, 'limit' => $limit, 'offset' => $offset))
                            );

                            $total = $employeeResponse->getTotalObjects();

                            $employeePidms = array_merge($employeePidms,
                                array_column($employeeResponse->getPayload(), 'pidm'));
                            $employeePayload = array_merge($employeePayload, $employeeResponse->getPayload());

                            $offset = $offset + $limit;

                        } while ($offset <= $total);

                        break;

                    case 'person':
                        $returnPerson = true;
                        $person = $this->callResource(
                            'person.read.collection.v2', array('options' => array('pidm' => $pidmStr))
                        );
                        $personPayload = $person->getPayload();
                        $personPidms = array_column($personPayload, 'pidm');
                        break;
                }
            }


            for ($i = 0; $i < sizeof($payload); $i++) {
                if ($returnEmployee) {

                    $payload[$i]['employee'] = null;
                    $keys = array_keys($employeePidms, $payload[$i]['pidm']);

                    if ($keys[0] !== null) {
                        $payload[$i]['employee'] = $employeePayload[$keys[0]];

                    }

                }
                if ($returnPerson) {

                    $payload[$i]['person'] = null;
                    $keys = array_keys($personPidms, $payload[$i]['pidm']);

                    if ($keys[0] !== null) {
                        $payload[$i]['person'] = $personPayload[$keys[0]];
                    }

                }
            }


        }
        return $payload;
    }

}
