<?php
/**
 * Created by PhpStorm.
 * User: q
 * Date: 1/24/19
 * Time: 11:27 AM
 */

namespace MiamiOH\RestngJob\Objects;

use MiamiOH\RESTngIlluminateIntegration\RESTngValidatorFactory;

class Job
{
    /**
     * @var string
     */
    private $muid = '';
    /**
     * @var string
     */
    private $positionNumber = '';
    /**
     * @var string
     */
    private $positionSuffix = '';
    /**
     * @var \DateTime
     */
    private $effectiveDate = null;
    /**
     * @var string
     */
    private $status = '';
    /**
     * @var string
     */
    private $description = '';
    /**
     * @var string
     */
    private $employeeClassificationCode = '';
    /**
     * @var string
     */
    private $payIdCode = '';
    /**
     * @var string
     */
    private $timeSheetChartOfAccountsCode = '';
    /**
     * @var string
     */
    private $timeSheetOrganization = '';
    /**
     * @var string
     */
    private $salaryTable = '';
    /**
     * @var string
     */
    private $salaryGrade = '';
    /**
     * @var string
     */
    private $salaryStep = '';
    /**
     * @var string
     */
    private $appointmentPercentage = '';
    /**
     * @var string
     */
    private $fullTimeEquivalency = '';
    /**
     * @var string
     */
    private $hoursPerDay = '';
    /**
     * @var string
     */
    private $hoursPerPay = '';
    /**
     * @var string
     */
    private $shift = '';
    /**
     * @var string
     */
    private $regularRate = '';
    /**
     * @var string
     */
    private $assignmentSalary = '';
    /**
     * @var string
     */
    private $factor = '';
    /**
     * @var string
     */
    private $annualSalary = '';
    /**
     * @var string
     */
    private $pays = '';
    /**
     * @var string
     */
    private $perPayDeferredAmount = '';
    /**
     * @var string
     */
    private $changeReasonCode = '';
    /**
     * @var string
     */
    private $salaryGroupCode = '';
    /**
     * @var string
     */
    private $employerCode = '';
    /**
     * @var string
     */
    private $longevityCode = '';
    /**
     * @var string
     */
    private $supervisorMuid = '';
    /**
     * @var string
     */
    private $supervisorPosition = '';
    /**
     * @var string
     */
    private $supervisorSuffix = '';

    /**
     * @var \DateTime
     */
    private $personnelChangeDate = null;

    /**
     * @var string
     */
    private $premiumPayCode = '';

    /**
     * @var string
     */
    private $timeEntryMethod = '';

    /**
     * @var string
     */
    private $timeEntryType = '';

    /**
     * @var string
     */
    private $timeInOutIndicator = '';

    /**
     * @var string
     */
    private $leaveReportMethod = '';

    /**
     * @var string
     */
    private $leaveReportPayId = '';

    /**
     * @var string
     */
    private $userId = '';

    /**
     * @var string
     */
    private $dataOrigin = '';

    /**
     * @var string
     */
    private $perPaySalary = '';

    /**
     * @return string
     */
    public function getMuid(): string
    {
        return $this->muid;
    }

    /**
     * @param string $muid
     * @return Job
     */
    public function setMuid(string $muid): Job
    {
        $this->muid = $muid;
        return $this;
    }

    /**
     * @return string
     */
    public function getPositionNumber(): string
    {
        return $this->positionNumber;
    }

    /**
     * @param string $positionNumber
     * @return Job
     */
    public function setPositionNumber(string $positionNumber): Job
    {
        $this->positionNumber = $positionNumber;
        return $this;
    }

    /**
     * @return string
     */
    public function getPositionSuffix(): string
    {
        return $this->positionSuffix;
    }

    /**
     * @param string $positionSuffix
     * @return Job
     */
    public function setPositionSuffix(string $positionSuffix): Job
    {
        $this->positionSuffix = $positionSuffix;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getEffectiveDate(): \DateTime
    {
        return $this->effectiveDate;
    }

    /**
     * @param string $effectiveDate
     * @return Job
     */
    public function setEffectiveDate(string $effectiveDate): Job
    {
        $this->effectiveDate = new \DateTime($effectiveDate);
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return Job
     */
    public function setStatus(string $status): Job
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Job
     */
    public function setDescription(string $description): Job
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmployeeClassificationCode(): string
    {
        return $this->employeeClassificationCode;
    }

    /**
     * @param string $employeeClassificationCode
     * @return Job
     */
    public function setEmployeeClassificationCode(string $employeeClassificationCode): Job
    {
        $this->employeeClassificationCode = $employeeClassificationCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getPayIdCode(): string
    {
        return $this->payIdCode;
    }

    /**
     * @param string $payIdCode
     * @return Job
     */
    public function setPayIdCode(string $payIdCode): Job
    {
        $this->payIdCode = $payIdCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getTimeSheetChartOfAccountsCode(): string
    {
        return $this->timeSheetChartOfAccountsCode;
    }

    /**
     * @param string $timeSheetChartOfAccountsCode
     * @return Job
     */
    public function setTimeSheetChartOfAccountsCode(string $timeSheetChartOfAccountsCode): Job
    {
        $this->timeSheetChartOfAccountsCode = $timeSheetChartOfAccountsCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getTimeSheetOrganization(): string
    {
        return $this->timeSheetOrganization;
    }

    /**
     * @param string $timeSheetOrganization
     * @return Job
     */
    public function setTimeSheetOrganization(string $timeSheetOrganization): Job
    {
        $this->timeSheetOrganization = $timeSheetOrganization;
        return $this;
    }

    /**
     * @return string
     */
    public function getSalaryTable(): string
    {
        return $this->salaryTable;
    }

    /**
     * @param string $salaryTable
     * @return Job
     */
    public function setSalaryTable(string $salaryTable): Job
    {
        $this->salaryTable = $salaryTable;
        return $this;
    }

    /**
     * @return string
     */
    public function getSalaryGrade(): string
    {
        return $this->salaryGrade;
    }

    /**
     * @param string $salaryGrade
     * @return Job
     */
    public function setSalaryGrade(string $salaryGrade): Job
    {
        $this->salaryGrade = $salaryGrade;
        return $this;
    }

    /**
     * @return string
     */
    public function getSalaryStep(): string
    {
        return $this->salaryStep;
    }

    /**
     * @param string $salaryStep
     * @return Job
     */
    public function setSalaryStep(string $salaryStep): Job
    {
        $this->salaryStep = $salaryStep;
        return $this;
    }

    /**
     * @return string
     */
    public function getAppointmentPercentage(): string
    {
        return $this->appointmentPercentage;
    }

    /**
     * @param string $appointmentPercentage
     * @return Job
     */
    public function setAppointmentPercentage(string $appointmentPercentage): Job
    {
        $this->appointmentPercentage = $appointmentPercentage;
        return $this;
    }

    /**
     * @return string
     */
    public function getFullTimeEquivalency(): string
    {
        return $this->fullTimeEquivalency;
    }

    /**
     * @param string $fullTimeEquivalency
     * @return Job
     */
    public function setFullTimeEquivalency(string $fullTimeEquivalency): Job
    {
        $this->fullTimeEquivalency = $fullTimeEquivalency;
        return $this;
    }

    /**
     * @return string
     */
    public function getHoursPerDay(): string
    {
        return $this->hoursPerDay;
    }

    /**
     * @param string $hoursPerDay
     * @return Job
     */
    public function setHoursPerDay(string $hoursPerDay): Job
    {
        $this->hoursPerDay = $hoursPerDay;
        return $this;
    }

    /**
     * @return string
     */
    public function getHoursPerPay(): string
    {
        return $this->hoursPerPay;
    }

    /**
     * @param string $hoursPerPay
     * @return Job
     */
    public function setHoursPerPay(string $hoursPerPay): Job
    {
        $this->hoursPerPay = $hoursPerPay;
        return $this;
    }

    /**
     * @return string
     */
    public function getShift(): string
    {
        return $this->shift;
    }

    /**
     * @param string $shift
     * @return Job
     */
    public function setShift(string $shift): Job
    {
        $this->shift = $shift;
        return $this;
    }

    /**
     * @return string
     */
    public function getRegularRate(): string
    {
        return $this->regularRate;
    }

    /**
     * @param string $regularRate
     * @return Job
     */
    public function setRegularRate(string $regularRate): Job
    {
        $this->regularRate = $regularRate;
        return $this;
    }

    /**
     * @return string
     */
    public function getAssignmentSalary(): string
    {
        return $this->assignmentSalary;
    }

    /**
     * @param string $assignmentSalary
     * @return Job
     */
    public function setAssignmentSalary(string $assignmentSalary): Job
    {
        $this->assignmentSalary = $assignmentSalary;
        return $this;
    }

    /**
     * @return string
     */
    public function getFactor(): string
    {
        return $this->factor;
    }

    /**
     * @param string $factor
     * @return Job
     */
    public function setFactor(string $factor): Job
    {
        $this->factor = $factor;
        return $this;
    }

    /**
     * @return string
     */
    public function getAnnualSalary(): string
    {
        return $this->annualSalary;
    }

    /**
     * @param string $annualSalary
     * @return Job
     */
    public function setAnnualSalary(string $annualSalary): Job
    {
        $this->annualSalary = $annualSalary;
        return $this;
    }

    /**
     * @return string
     */
    public function getPays(): string
    {
        return $this->pays;
    }

    /**
     * @param string $pays
     * @return Job
     */
    public function setPays(string $pays): Job
    {
        $this->pays = $pays;
        return $this;
    }

    /**
     * @return string
     */
    public function getPerPayDeferredAmount(): string
    {
        return $this->perPayDeferredAmount;
    }

    /**
     * @param string $perPayDeferredAmount
     * @return Job
     */
    public function setPerPayDeferredAmount(string $perPayDeferredAmount): Job
    {
        $this->perPayDeferredAmount = $perPayDeferredAmount;
        return $this;
    }

    /**
     * @return string
     */
    public function getChangeReasonCode(): string
    {
        return $this->changeReasonCode;
    }

    /**
     * @param string $changeReasonCode
     * @return Job
     */
    public function setChangeReasonCode(string $changeReasonCode): Job
    {
        $this->changeReasonCode = $changeReasonCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getSalaryGroupCode(): string
    {
        return $this->salaryGroupCode;
    }

    /**
     * @param string $salaryGroupCode
     * @return Job
     */
    public function setSalaryGroupCode(string $salaryGroupCode): Job
    {
        $this->salaryGroupCode = $salaryGroupCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmployerCode(): string
    {
        return $this->employerCode;
    }

    /**
     * @param string $employerCode
     * @return Job
     */
    public function setEmployerCode(string $employerCode): Job
    {
        $this->employerCode = $employerCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getLongevityCode(): string
    {
        return $this->longevityCode;
    }

    /**
     * @param string $longevityCode
     * @return Job
     */
    public function setLongevityCode(string $longevityCode): Job
    {
        $this->longevityCode = $longevityCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getSupervisorMuid(): string
    {
        return $this->supervisorMuid;
    }

    /**
     * @param string $supervisorMuid
     * @return Job
     */
    public function setSupervisorMuid(string $supervisorMuid): Job
    {
        $this->supervisorMuid = $supervisorMuid;
        return $this;
    }

    /**
     * @return string
     */
    public function getSupervisorPosition(): string
    {
        return $this->supervisorPosition;
    }

    /**
     * @param string $supervisorPosition
     * @return Job
     */
    public function setSupervisorPosition(string $supervisorPosition): Job
    {
        $this->supervisorPosition = $supervisorPosition;
        return $this;
    }

    /**
     * @return string
     */
    public function getSupervisorSuffix(): string
    {
        return $this->supervisorSuffix;
    }

    /**
     * @param string $supervisorSuffix
     * @return Job
     */
    public function setSupervisorSuffix(string $supervisorSuffix): Job
    {
        $this->supervisorSuffix = $supervisorSuffix;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getPersonnelChangeDate(): \DateTime
    {
        return $this->personnelChangeDate;
    }

    /**
     * @param string $personnelChangeDate
     * @return Job
     */
    public function setPersonnelChangeDate(string $personnelChangeDate): Job
    {
        $this->personnelChangeDate = new \DateTime($personnelChangeDate);
        return $this;
    }

    /**
     * @return string
     */
    public function getPremiumPayCode(): string
    {
        return $this->premiumPayCode;
    }

    /**
     * @param string $premiumPayCode
     * @return Job
     */
    public function setPremiumPayCode(string $premiumPayCode): Job
    {
        $this->premiumPayCode = $premiumPayCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getTimeEntryMethod(): string
    {
        return $this->timeEntryMethod;
    }

    /**
     * @param string $timeEntryMethod
     * @return Job
     */
    public function setTimeEntryMethod(string $timeEntryMethod): Job
    {
        $this->timeEntryMethod = $timeEntryMethod;
        return $this;
    }

    /**
     * @return string
     */
    public function getTimeEntryType(): string
    {
        return $this->timeEntryType;
    }

    /**
     * @param string $timeEntryType
     * @return Job
     */
    public function setTimeEntryType(string $timeEntryType): Job
    {
        $this->timeEntryType = $timeEntryType;
        return $this;
    }

    /**
     * @return string
     */
    public function getTimeInOutIndicator(): string
    {
        return $this->timeInOutIndicator;
    }

    /**
     * @param string $timeInOutIndicator
     * @return Job
     */
    public function setTimeInOutIndicator(string $timeInOutIndicator): Job
    {
        $this->timeInOutIndicator = $timeInOutIndicator;
        return $this;
    }

    /**
     * @return string
     */
    public function getLeaveReportMethod(): string
    {
        return $this->leaveReportMethod;
    }

    /**
     * @param string $leaveReportMethod
     * @return Job
     */
    public function setLeaveReportMethod(string $leaveReportMethod): Job
    {
        $this->leaveReportMethod = $leaveReportMethod;
        return $this;
    }

    /**
     * @return string
     */
    public function getLeaveReportPayId(): string
    {
        return $this->leaveReportPayId;
    }

    /**
     * @param string $leaveReportPayId
     * @return Job
     */
    public function setLeaveReportPayId(string $leaveReportPayId): Job
    {
        $this->leaveReportPayId = $leaveReportPayId;
        return $this;
    }

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->userId;
    }

    /**
     * @param string $userId
     * @return Job
     */
    public function setUserId(string $userId): Job
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * @return string
     */
    public function getDataOrigin(): string
    {
        return $this->dataOrigin;
    }

    /**
     * @param string $dataOrigin
     * @return Job
     */
    public function setDataOrigin(string $dataOrigin): Job
    {
        $this->dataOrigin = $dataOrigin;
        return $this;
    }

    /**
     * @return string
     */
    public function getPerPaySalary(): string
    {
        return $this->perPaySalary;
    }

    /**
     * @param string $perPaySalary
     * @return Job
     */
    public function setPerPaySalary(string $perPaySalary): Job
    {
        $this->perPaySalary = $perPaySalary;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getActivityDate(): \DateTime
    {
        return new \DateTime('now');
    }

    /**
     * @var array
     */
    private static $rules = [
        'muid' => ['required'], // delegate validation to ws-muid
        'positionNumber' => ['required', 'regex:/^\d{1,6}$/'],
        'positionSuffix' => ['required', 'regex:/^\d{1,2}$/'],
        'effectiveDate' => ['required', 'date'],
        'status' => ['required', 'max:1'],
        'employeeClassificationCode' => ['required', 'max:2'],
        'payIdCode' => ['required', 'max:2'],
        'timeSheetOrganization' => ['required', 'max:6'],
        'appointmentPercentage' => ['required', 'numeric', 'regex:/^\d{1,3}(\.\d*)?$/'],
        'hoursPerDay' => ['required', 'numeric', 'regex:/^\d{1,4}(\.\d*)?$/'],
        'hoursPerPay' => ['required', 'numeric', 'regex:/^\d{1,4}(\.\d*)?$/'],
        'shift' => ['required', 'max:1'],
        'assignmentSalary' => ['required', 'numeric', 'regex:/^\d{1,9}(\.\d*)?$/'],
        'factor' => ['required', 'numeric', 'regex:/^\d{1,2}(\.\d*)?$/'],
        'annualSalary' => ['required', 'numeric', 'regex:/^\d{1,9}(\.\d*)?$/'],
        'perPaySalary' => ['required', 'numeric', 'regex:/^\d{1,9}(\.\d*)?$/'],
        'pays' => ['required', 'numeric', 'regex:/^\d{1,2}(\.\d*)?$/'],
        'perPayDeferredAmount' => ['required', 'numeric', 'regex:/^\d{1,9}(\.\d*)?$/'],
        'salaryGroupCode' => ['required', 'max:6'],
        'timeEntryMethod' => ['required', 'max:1'],
        'timeEntryType' => ['required', 'max:1'],
        'timeInOutIndicator' => ['required', 'max:1'],
        'leaveReportMethod' => ['required', 'max:1'],
        'leaveReportPayId' => ['required', 'max:2'],
        'personnelChangeDate' => ['required', 'date'],
        'timeSheetChartOfAccountsCode' => ['max:1'],
        'salaryTable' => ['max:2'],
        'salaryGrade' => ['max:5'],
        'salaryStep' => ['numeric', 'regex:/^\d{1,3}$/'],
        'fullTimeEquivalency' => ['numeric', 'regex:/^\d{1,3}(\.\d*)?$/'],
        'regularRate' => ['numeric', 'regex:/^\d{1,5}(\.\d*)?$/'],
        'changeReasonCode' => ['max:5'],
        'employerCode' => ['max:4'],
        'longevityCode' => ['max:4'],
        'supervisorMuid' => [], // delegate validation to ws-muid
        'supervisorPosition' => ['max:6'],
        'supervisorSuffix' => ['max:2'],
        'premiumPayCode' => ['max:4'],
        'description' => ['max:30'],
        'userId' => ['max:30'],
        'dataOrigin' => ['max:30'],
    ];

    private $modifiedAttributes = [];

    /**
     * @return array
     */
    public function getModifiedAttributes(): array
    {
        return $this->modifiedAttributes;
    }

    /**
     * @param string $attributes
     */
    public function addModifiedAttributes(string $attributes): void
    {
        $this->modifiedAttributes[$attributes] = true;
    }

    /**
     * @param array $data
     * @return Job
     * @throws \Exception
     */
    public static function fromArray(array $data): self
    {
        self::validateArray($data);

        $thisInstance = new self();

        try {
            foreach ($data as $key => $val) {
                $thisInstance->{'set' . ucfirst($key)}($val);
                $thisInstance->addModifiedAttributes($key);
            }
        } catch (\Exception $e) {
            throw new \Exception("Invalid key for Job: $key" . $e->getMessage());
        }

        return $thisInstance;
    }

    /**
     * @param array $data
     * @throws \Exception
     */
    public static function validateArray(array $data): void
    {
        $validator = RESTngValidatorFactory::make($data, self::$rules);

        if ($validator->fails()) {
            throw new \Exception(
                'Validation failed. '
                . implode(' ', $validator->errors()->all())
            );
        }
    }
}
