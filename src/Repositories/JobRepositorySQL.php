<?php
/**
 * Created by PhpStorm.
 * User: q
 * Date: 1/25/19
 * Time: 11:25 AM
 */

namespace MiamiOH\RestngJob\Repositories;

use MiamiOH\RestngJob\EloquentModels\JobEloquentModel;
use MiamiOH\RestngJob\Objects\Job;

class JobRepositorySQL implements JobRepository
{
    /**
     * @var MUIDRepository
     */
    private $muidRepository;

    /**
     * JobRepositorySQL constructor.
     * @param MUIDRepository $muidRepository
     */
    public function __construct(MUIDRepository $muidRepository)
    {
        $this->muidRepository = $muidRepository;
    }

    /**
     * @param Job $job
     * @return bool
     * @throws \Exception
     */
    public function create(
        Job $job
    ): bool
    {
        $employeeId = strtoupper($job->getMuid());
        $supervisorId = strtoupper($job->getSupervisorMuid());

        $muids = $this->muidRepository->readMUIDs([$employeeId, $supervisorId]);

        $employeePidm = empty($muids[$employeeId]) ? '' : $muids[$employeeId]['pidm'];
        $supervisorPidm = empty($muids[$supervisorId]) ? '' : $muids[$supervisorId]['pidm'];

        if (empty($employeePidm)) {
            throw new \Exception("Cannot find employee pidm for muid '$employeeId'.");
        }

        $modifiedAttributes = $job->getModifiedAttributes();

        if (isset($modifiedAttributes['supervisorMuid']) && empty($supervisorPidm)) {
            throw new \Exception("Cannot find supervisor pidm for muid '$supervisorId'.");
        }

        JobEloquentModel::create([
            'nbrjobs_pidm' => $employeePidm,
            'nbrjobs_posn' => $job->getPositionNumber(),
            'nbrjobs_suff' => $job->getPositionSuffix(),
            'nbrjobs_effective_date' => $job->getEffectiveDate()->format('Y-m-d H:i:s'),
            'nbrjobs_status' => $job->getStatus(),
            'nbrjobs_desc' => $job->getDescription(),
            'nbrjobs_ecls_code' => $job->getEmployeeClassificationCode(),
            'nbrjobs_pict_code' => $job->getPayIdCode(),
            'nbrjobs_coas_code_ts' => $job->getTimeSheetChartOfAccountsCode(),
            'nbrjobs_orgn_code_ts' => $job->getTimeSheetOrganization(),
            'nbrjobs_sal_table' => $job->getSalaryTable(),
            'nbrjobs_sal_grade' => $job->getSalaryGrade(),
            'nbrjobs_sal_step' => $job->getSalaryStep(),
            'nbrjobs_appt_pct' => $job->getAppointmentPercentage(),
            'nbrjobs_fte' => $job->getFullTimeEquivalency(),
            'nbrjobs_hrs_day' => $job->getHoursPerDay(),
            'nbrjobs_hrs_pay' => $job->getHoursPerPay(),
            'nbrjobs_shift' => $job->getShift(),
            'nbrjobs_reg_rate' => $job->getRegularRate(),
            'nbrjobs_assgn_salary' => $job->getAssignmentSalary(),
            'nbrjobs_factor' => $job->getFactor(),
            'nbrjobs_ann_salary' => $job->getAnnualSalary(),
            'nbrjobs_per_pay_salary' => $job->getPerPaySalary(),
            'nbrjobs_pays' => $job->getPays(),
            'nbrjobs_per_pay_defer_amt' => $job->getPerPayDeferredAmount(),
            'nbrjobs_jcre_code' => $job->getChangeReasonCode(),
            'nbrjobs_sgrp_code' => $job->getSalaryGroupCode(),
            'nbrjobs_empr_code' => $job->getEmployerCode(),
            'nbrjobs_lgcd_code' => $job->getLongevityCode(),
            'nbrjobs_pers_chg_date' => $job->getPersonnelChangeDate()->format('Y-m-d H:i:s'),
            'nbrjobs_pcat_code' => $job->getPremiumPayCode(),
            'nbrjobs_time_entry_method' => $job->getTimeEntryMethod(),
            'nbrjobs_time_entry_type' => $job->getTimeEntryType(),
            'nbrjobs_time_in_out_ind' => $job->getTimeInOutIndicator(),
            'nbrjobs_leav_rept_method' => $job->getLeaveReportMethod(),
            'nbrjobs_pict_code_leav_rept' => $job->getLeaveReportPayId(),
            'nbrjobs_supervisor_pidm' => $supervisorPidm,
            'nbrjobs_supervisor_posn' => $job->getSupervisorPosition(),
            'nbrjobs_supervisor_suff' => $job->getSupervisorSuffix(),
            'nbrjobs_user_id' => $job->getUserId(),
            'nbrjobs_data_origin' => $job->getDataOrigin(),
            'nbrjobs_activity_date' => $job->getActivityDate()->format('Y-m-d H:i:s'),
        ]);

        return true;
    }

    /**
     * @param Job $job
     * @return bool
     * @throws \Exception
     */
    public function update(
        Job $job
    ): bool
    {
        $employeeId = strtoupper($job->getMuid());
        $supervisorId = strtoupper($job->getSupervisorMuid());

        $muids = $this->muidRepository->readMUIDs([$employeeId, $supervisorId]);

        $employeePidm = empty($muids[$employeeId]) ? '' : $muids[$employeeId]['pidm'];
        $supervisorPidm = empty($muids[$supervisorId]) ? '' : $muids[$supervisorId]['pidm'];

        if (empty($employeePidm)) {
            throw new \Exception("Cannot find employee pidm for muid '$employeeId'.");
        }

        $modifiedAttributes = $job->getModifiedAttributes();

        $newModel = JobEloquentModel::firstOrNew(
            [
                'nbrjobs_pidm' => $employeePidm,
                'nbrjobs_posn' => $job->getPositionNumber(),
                'nbrjobs_suff' => $job->getPositionSuffix(),
                'nbrjobs_effective_date' => $job->getEffectiveDate()->format('Y-m-d H:i:s'),
            ]);

        if (isset($modifiedAttributes['supervisorMuid'])) {
            if (empty($supervisorPidm)) {
                throw new \Exception("Cannot find supervisor pidm for muid '$supervisorId'.");
            }
            $newModel->nbrjobs_supervisor_pidm = $supervisorPidm;
        }

        if (isset($modifiedAttributes['supervisorPosition'])) {
            $newModel->nbrjobs_supervisor_posn = $job->getSupervisorPosition();
        }

        if (isset($modifiedAttributes['supervisorSuffix'])) {
            $newModel->nbrjobs_supervisor_suff = $job->getSupervisorSuffix();
        }

        if (isset($modifiedAttributes['status'])) {
            $newModel->nbrjobs_status = $job->getStatus();
        };

        if (isset($modifiedAttributes['status'])) {
            $newModel->nbrjobs_status = $job->getStatus();
        }

        if (isset($modifiedAttributes['description'])) {
            $newModel->nbrjobs_desc = $job->getDescription();
        }

        if (isset($modifiedAttributes['employeeClassificationCode'])) {
            $newModel->nbrjobs_ecls_code = $job->getEmployeeClassificationCode();
        }

        if (isset($modifiedAttributes['payIdCode'])) {
            $newModel->nbrjobs_pict_code = $job->getPayIdCode();
        }

        if (isset($modifiedAttributes['timeSheetChartOfAccountsCode'])) {
            $newModel->nbrjobs_coas_code_ts = $job->getTimeSheetChartOfAccountsCode();
        }

        if (isset($modifiedAttributes['timeSheetOrganization'])) {
            $newModel->nbrjobs_orgn_code_ts = $job->getTimeSheetOrganization();
        }

        if (isset($modifiedAttributes['salaryTable'])) {
            $newModel->nbrjobs_sal_table = $job->getSalaryTable();
        }

        if (isset($modifiedAttributes['salaryGrade'])) {
            $newModel->nbrjobs_sal_grade = $job->getSalaryGrade();
        }

        if (isset($modifiedAttributes['salaryStep'])) {
            $newModel->nbrjobs_sal_step = $job->getSalaryStep();
        }

        if (isset($modifiedAttributes['appointmentPercentage'])) {
            $newModel->nbrjobs_appt_pct = $job->getAppointmentPercentage();
        }

        if (isset($modifiedAttributes['fullTimeEquivalency'])) {
            $newModel->nbrjobs_fte = $job->getFullTimeEquivalency();
        }

        if (isset($modifiedAttributes['hoursPerDay'])) {
            $newModel->nbrjobs_hrs_day = $job->getHoursPerDay();
        }

        if (isset($modifiedAttributes['hoursPerPay'])) {
            $newModel->nbrjobs_hrs_pay = $job->getHoursPerPay();
        }

        if (isset($modifiedAttributes['shift'])) {
            $newModel->nbrjobs_shift = $job->getShift();
        }

        if (isset($modifiedAttributes['regularRate'])) {
            $newModel->nbrjobs_reg_rate = $job->getRegularRate();
        }

        if (isset($modifiedAttributes['assignmentSalary'])) {
            $newModel->nbrjobs_assgn_salary = $job->getAssignmentSalary();
        }

        if (isset($modifiedAttributes['factor'])) {
            $newModel->nbrjobs_factor = $job->getFactor();
        }

        if (isset($modifiedAttributes['annualSalary'])) {
            $newModel->nbrjobs_ann_salary = $job->getAnnualSalary();
        }

        if (isset($modifiedAttributes['perPaySalary'])) {
            $newModel->nbrjobs_per_pay_salary = $job->getPerPaySalary();
        }

        if (isset($modifiedAttributes['pays'])) {
            $newModel->nbrjobs_pays = $job->getPays();
        }

        if (isset($modifiedAttributes['perPayDeferredAmount'])) {
            $newModel->nbrjobs_per_pay_defer_amt = $job->getPerPayDeferredAmount();
        }

        if (isset($modifiedAttributes['changeReasonCode'])) {
            $newModel->nbrjobs_jcre_code = $job->getChangeReasonCode();
        }

        if (isset($modifiedAttributes['salaryGroupCode'])) {
            $newModel->nbrjobs_sgrp_code = $job->getSalaryGroupCode();
        }

        if (isset($modifiedAttributes['employerCode'])) {
            $newModel->nbrjobs_empr_code = $job->getEmployerCode();
        }

        if (isset($modifiedAttributes['longevityCode'])) {
            $newModel->nbrjobs_lgcd_code = $job->getLongevityCode();
        }

        if (isset($modifiedAttributes['personnelChangeDate'])) {
            $newModel->nbrjobs_pers_chg_date = $job->getPersonnelChangeDate()->format('Y-m-d H:i:s');
        }

        if (isset($modifiedAttributes['premiumPayCode'])) {
            $newModel->nbrjobs_pcat_code = $job->getPremiumPayCode();
        }

        if (isset($modifiedAttributes['timeEntryMethod'])) {
            $newModel->nbrjobs_time_entry_method = $job->getTimeEntryMethod();
        }

        if (isset($modifiedAttributes['timeEntryType'])) {
            $newModel->nbrjobs_time_entry_type = $job->getTimeEntryType();
        }

        if (isset($modifiedAttributes['timeInOutIndicator'])) {
            $newModel->nbrjobs_time_in_out_ind = $job->getTimeInOutIndicator();
        }

        if (isset($modifiedAttributes['leaveReportMethod'])) {
            $newModel->nbrjobs_leav_rept_method = $job->getLeaveReportMethod();
        }

        if (isset($modifiedAttributes['leaveReportPayId'])) {
            $newModel->nbrjobs_pict_code_leav_rept = $job->getLeaveReportPayId();
        }

        $newModel->nbrjobs_user_id = $job->getUserId();
        $newModel->nbrjobs_data_origin = $job->getDataOrigin();
        $newModel->nbrjobs_activity_date = $job->getActivityDate()->format('Y-m-d H:i:s');

        $newModel->save();

        return true;
    }
}
