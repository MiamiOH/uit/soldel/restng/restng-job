<?php
/**
 * Created by PhpStorm.
 * User: q
 * Date: 1/25/19
 * Time: 11:23 AM
 */

namespace MiamiOH\RestngJob\Repositories;

use MiamiOH\RestngJob\Objects\Job;

interface JobRepository
{
    public function create(Job $job): bool;

    public function update(Job $job): bool;
}